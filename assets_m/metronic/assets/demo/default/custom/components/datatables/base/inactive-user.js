//== Class definition

var DatatableJsonRemoteDemo = function () {
	//== Private functions

	// basic demo
	var demo = function () {

		var datatable = $('.m_datatable').mDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: {
				  read: {
					// sample GET method
					method: 'GET',
					url: '/business/users/inactive_ajax_list',
					map: function(raw) {
					  // sample data mapping
					  var dataSet = raw;
					  if (typeof raw.data !== 'undefined') {
						dataSet = raw.data;
					  }
					  return dataSet;
					},
				  },
				},
				pageSize: 10,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			  },

			// layout definition
			layout: {
				theme: 'default', // datatable theme
				class: '', // custom wrapper class
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,
			toolbar: {
				// toolbar items
				items: {
				  // pagination
				  pagination: {
					// page size select
					pageSizeSelect: [10, 20, 30, 50, 100],
				  },
				},
			  },

			search: {
				input: $('#generalSearch')
			},

			// columns definition
			columns: [{
				field: "id",
				title: "#",
				width: 50,
				sortable: true,
				selector: false,
			}, {
				field: "social_id",
                title: "SOCIAL ID",
			}, {
				field: "name",
                title: "Name",
			}, {
				field: "email",
				title: "Email",
			},{
				field: "phone",
				title: "Phone",
				template: function(row) {
                    // callback function support for column rendering
                    return row.country_code + ' ' + row.phone;
                  },
			},{
				field: "gender",
				title: "Sex",
			},{
				field: "last_login",
				title: "Last Login",
				responsive: {visible: 'lg'}
			},{
				field: "is_login",
				title: "Is Login",
				template: function(row) {
                    if(row.is_login == 1)
                        return '<a class="text-success">Login</a>';
                    else
                        return '<a class="text-danger">Logout</a>';
                  },
			}]
		});

		var query = datatable.getDataSourceQuery();

		$('#m_form_status').on('change', function () {
			datatable.search($(this).val(), 'Status');
		}).val(typeof query.Status !== 'undefined' ? query.Status : '');

		$('#m_form_type').on('change', function () {
			datatable.search($(this).val(), 'Type');
		}).val(typeof query.Type !== 'undefined' ? query.Type : '');

		$('#m_form_status, #m_form_type').selectpicker();

	};

	return {
		// public functions
		init: function () {
			demo();
		}
	};
}();

jQuery(document).ready(function () {
	DatatableJsonRemoteDemo.init();
});