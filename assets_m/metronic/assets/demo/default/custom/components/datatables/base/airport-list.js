//== Class definition

var DatatableJsonRemoteDemo = function () {
	//== Private functions

	// basic demo
	var demo = function () {

		var datatable = $('.m_datatable').mDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: {
				  read: {
					// sample GET method
					method: 'GET',
					//url: '/business/airportfare/ajax_list',
					map: function(raw) {
					  // sample data mapping
					  var dataSet = raw;
					  if (typeof raw.data !== 'undefined') {
						dataSet = raw.data;
					  }
					  return dataSet;
					},
				  },
				},
				pageSize: 10,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			  },

			// layout definition
			layout: {
				theme: 'default', // datatable theme
				class: '', // custom wrapper class
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,
			toolbar: {
				// toolbar items
				items: {
				  // pagination
				  pagination: {
					// page size select
					pageSizeSelect: [10, 20, 30, 50, 100],
				  },
				},
			  },

			search: {
				input: $('#generalSearch')
			},

			// columns definition
			columns: [{
				field: "id",
				title: "#",
				width: 50,
				sortable: true,
				selector: false,
			}, {
				field: "area",
                title: "Area",
                template: function(row) {
                    // callback function support for column rendering
                    return row.fname + ' ' + row.lname;
                  },
			}, {
				field: "email",
				title: "Email",
			}, {
				field: "phone",
				title: "Phone",
				template: function(row) {
                    // callback function support for column rendering
                    return row.country_code + ' ' + row.phone;
                  },
			},{
				field: "app_version",
				title: "App Ver.",
				width:50,
				responsive: {visible: 'lg'}
			}, {
				field: "gender",
				title: "Sex",
			},
			{
				field: "car_type",
				title: "Car Type",
				responsive: {visible: 'lg'}
			},
			{
				field: "state",
				title: "State",
				responsive: {visible: 'lg'}
			}, {
				field: "is_service",
				title: "Is Service",
				template: function(row) {
                    if(row.is_service == 1)
                        return "On";
                    else
                        return "Off";
                  },
			},  {
				field: "is_free",
				title: "Is Free",
				template: function(row) {
                    if(row.is_free == 1)
                        return "Yes";
                    else
                        return "No";
                  },
			}, {
				field: "is_login",
				title: "Is Login",
				template: function(row) {
                    if(row.is_login == 1)
                        return "Login";
                    else
                        return "Logout";
                  },
			}, {
				field: "doc_status",
				title: "Doc Status",
				// callback function support for column rendering
				template: function (row) {
					if(row.doc_status == "Waiting")
                        return '\<a href="edit_document/'+row.id+'" class="text-warning">Waiting</a>';
                    else if(row.doc_status == "Processing")
                        return '\<a href="edit_document/'+row.id+'" class="text-success">Processing</a>';
                    else if(row.doc_status == "Training")
                        return '\<a href="edit_document/'+row.id+'" class="text-pink">Training</a>';
                    else
					    return '\<a href="edit_document/'+row.id+'" class="text-info">Complete</a>';
				}
			}, {
				field: "is_active",
				title: "Is Active",
				template: function(row) {
                    if(row.is_active == 1)
                        return '<a href="change_status/'+row.id+'/0"  onclick="return confirm(\'Are you sure you want to Inactive this driver?\');" class="text-success">Y</a>';
                    else
                        return '<a href="change_status/'+row.id+'/1"  onclick="return confirm(\'Are you sure you want to Active this driver?\');" class="text-warning">N</a>';
                  },
			},  {
				field: "Actions",
				width: 100,
				title: "Actions",
				sortable: false,
				overflow: 'visible',
				template: function (row, index, datatable) {
					var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                    return '\
                        <a href="view/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Download">\
							<i class="la la-eye"></i>\
                        </a>\
                        <a href="map/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Download">\
							<i class="la la-map"></i>\
						</a>\
						<a href="edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Download">\
							<i class="la la-edit"></i>\
                        </a>\
						<a href="remove/'+row.id+'" onclick="return confirm(\'Are you sure you want to delete cartype ?\');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Edit settings">\
							<i class="la la-trash"></i>\
						</a>\
					';
				}
			}]
		});

		var query = datatable.getDataSourceQuery();

		$('#m_form_status').on('change', function () {
			datatable.search($(this).val(), 'Status');
		}).val(typeof query.Status !== 'undefined' ? query.Status : '');

		$('#m_form_type').on('change', function () {
			datatable.search($(this).val(), 'Type');
		}).val(typeof query.Type !== 'undefined' ? query.Type : '');

		$('#m_form_status, #m_form_type').selectpicker();

	};

	return {
		// public functions
		init: function () {
			demo();
		}
	};
}();

jQuery(document).ready(function () {
	DatatableJsonRemoteDemo.init();
});