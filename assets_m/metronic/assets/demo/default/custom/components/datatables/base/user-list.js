//== Class definition

var DatatableJsonRemoteDemo = function () {
	//== Private functions

	// basic demo
	var demo = function () {

		var datatable = $('.m_datatable').mDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: {
				  read: {
					// sample GET method
					method: 'GET',
					url: '/business/users/ajax_list',
					map: function(raw) {
					  // sample data mapping
					  var dataSet = raw;
					  if (typeof raw.data !== 'undefined') {
						dataSet = raw.data;
					  }
					  return dataSet;
					},
				  },
				},
				pageSize: 10,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			  },

			// layout definition
			layout: {
				theme: 'default', // datatable theme
				class: '', // custom wrapper class
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,
			toolbar: {
				// toolbar items
				items: {
				  // pagination
				  pagination: {
					// page size select
					pageSizeSelect: [10, 20, 30, 50, 100],
				  },
				},
			  },

			search: {
				input: $('#generalSearch')
			},

			// columns definition
			columns: [{
				field: "id",
				title: "#",
				width: 50,
				sortable: true,
				selector: false,
			}, {
				field: "social_id",
                title: "SOCIAL ID",
			}, {
				field: "name",
                title: "Name",
			}, {
				field: "email",
				title: "Email",
			}, {
				field: "phone",
				title: "Phone",
				template: function(row) {
                    // callback function support for column rendering
                    return row.country_code + ' ' + row.phone;
                  },
			},{
				field: "app_version",
				title: "App Ver.",
				width:50,
				responsive: {visible: 'lg'}
			}, {
				field: "gender",
				title: "Sex",
			},
			{
				field: "last_login",
				title: "Last Login",
				responsive: {visible: 'lg'}
			},
			{
				field: "order_block",
				title: "Order block",
				template: function (row) {
					if(row.order_block == 1)
                        return '<span class="text text-danger"><i class="la la-ban"></i>Y</span>';
                    else
					    return '<span class="text text-success"><i class="la la-check"></i>N</span>';
				}
			}, {
				field: "is_login",
				title: "Is Login",
				template: function(row) {
                    if(row.is_login == 1)
                        return '<a class="text-success">Login</a>';
                    else
                        return '<a class="text-danger">Logout</a>';
                  },
			}, {
				field: "is_active",
				title: "Is Active",
				template: function(row) {
                    if(row.is_active == 1)
                        return '<a href="change_status/'+row.id+'/0"  onclick="return confirm(\'Are you sure you want to Inactive this user?\');" class="text-success">Active</a>';
                    else
                        return '<a href="change_status/'+row.id+'/1"  onclick="return confirm(\'Are you sure you want to Active this user?\');" class="text-warning">Inactive</a>';
                  },
			},  {
				field: "Actions",
				width: 100,
				title: "Actions",
				sortable: false,
				overflow: 'visible',
				template: function (row, index, datatable) {
					var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                    return '\
                        <a href="view/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View">\
							<i class="la la-eye"></i>\
                        </a>\
						<a href="edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\
							<i class="la la-edit"></i>\
                        </a>\
						<a href="remove/'+row.id+'" onclick="return confirm(\'Are you sure you want to delete user ?\');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\
							<i class="la la-trash"></i>\
						</a>\
					';
				}
			}]
		});

		var query = datatable.getDataSourceQuery();

		$('#m_form_status').on('change', function () {
			datatable.search($(this).val(), 'Status');
		}).val(typeof query.Status !== 'undefined' ? query.Status : '');

		$('#m_form_type').on('change', function () {
			datatable.search($(this).val(), 'Type');
		}).val(typeof query.Type !== 'undefined' ? query.Type : '');

		$('#m_form_status, #m_form_type').selectpicker();

	};

	return {
		// public functions
		init: function () {
			demo();
		}
	};
}();

jQuery(document).ready(function () {
	DatatableJsonRemoteDemo.init();
});