//== Class definition

var DatatableJsonRemoteDemo = function () {
	//== Private functions

	// basic demo
	var demo = function () {

		var datatable = $('.m_datatable').mDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: {
				  read: {
					// sample GET method
					method: 'GET',
					url: '/business/cartype/ajax_list',
					map: function(raw) {
					  // sample data mapping
					  var dataSet = raw;
					  if (typeof raw.data !== 'undefined') {
						dataSet = raw.data;
					  }
					  return dataSet;
					},
				  },
				},
				pageSize: 10,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			  },

			// layout definition
			layout: {
				theme: 'default', // datatable theme
				class: '', // custom wrapper class
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,
			toolbar: {
				// toolbar items
				items: {
				  // pagination
				  pagination: {
					// page size select
					pageSizeSelect: [10, 20, 30, 50, 100],
				  },
				},
			  },

			search: {
				input: $('#generalSearch')
			},

			// columns definition
			columns: [{
				field: "id",
				title: "#",
				width: 50,
				sortable: true,
				selector: false,
			}, {
				field: "region",
				title: "Region"
			}, {
				field: "type",
				title: "Car Type",
			}, {
				field: "base_fare",
				title: "Base Fare",
				width: 110
			}, {
				field: "per_minute",
				title: "Per Minute",
				responsive: {visible: 'lg'}
			}, {
				field: "per_km",
				title: "Per KM",
				width:50,
				responsive: {visible: 'lg'}
			}, {
				field: "min_fare",
				title: "MIN. Fare",
				width:70
			},
			{
				field: "cancellation",
				title: "Cancellation",
				responsive: {visible: 'lg'}
			},
			{
				field: "booking_fare",
				title: "Booking Fare",
				responsive: {visible: 'lg'}
			}, {
				field: "passenger_levy",
				title: "Pass. Levy",
				responsive: {visible: 'lg'}
			},  {
				field: "splitting_ratio",
				title: "Splitting Ratio",
				width:50,
				responsive: {visible: 'lg'}
			},  {
				field: "is_active",
				title: "Is Active",
				// callback function support for column rendering
				template: function (row) {
					var status = {
						0: {'title': 'Inactive', 'state': 'danger'},
						1: {'title': 'Active', 'state': 'primary'}
					};
					return '<span class="m-badge m-badge--' + status[row.is_active].state + ' m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-' + status[row.is_active].state + '">' + status[row.is_active].title + '</span>';
				}
			}, {
				field: "Actions",
				width: 100,
				title: "Actions",
				sortable: false,
				overflow: 'visible',
				template: function (row, index, datatable) {
					var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
					return '\
						<a href="edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Download">\
							<i class="la la-edit"></i>\
						</a>\
						<a href="remove/'+row.id+'" onclick="return confirm(\'Are you sure you want to delete cartype ?\');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Edit settings">\
							<i class="la la-trash"></i>\
						</a>\
					';
				}
			}]
		});

		var query = datatable.getDataSourceQuery();

		$('#m_form_status').on('change', function () {
			datatable.search($(this).val(), 'Status');
		}).val(typeof query.Status !== 'undefined' ? query.Status : '');

		$('#m_form_type').on('change', function () {
			datatable.search($(this).val(), 'Type');
		}).val(typeof query.Type !== 'undefined' ? query.Type : '');

		$('#m_form_status, #m_form_type').selectpicker();

	};

	return {
		// public functions
		init: function () {
			demo();
		}
	};
}();

jQuery(document).ready(function () {
	DatatableJsonRemoteDemo.init();
});