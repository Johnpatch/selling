var DefaultDatatableDemo = function () {
	//== Private functions

	// basic demo
	var demo = function () {

		var datatable = $('.m_datatable').mDatatable({
			data: {
				type: 'remote',
				source: {
					read: {
						url: "/business/staff/ajax_listing"
					}
				},
				pageSize: 20,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true
			},

			layout: {
				theme: 'default',
				class: '',
				scroll: true,
				height: 550,
				footer: false
			},

			sortable: true,

			filterable: false,

			pagination: true,

			search: {
				input: $('#generalSearch')
			},

			columns: [{
				field: "id",
				title: "#",
				//sortable: false,
				width: 40,
				//selector: {class: 'm-checkbox--solid m-checkbox--brand'}
			}, {
				field: "name",
				title: "Name",
				sortable: 'asc',
				filterable: false,
				width: 70
				
			}, {
				field: "email",
				title: "Email",
				width: 230,
				responsive: {visible: 'lg'}
			}, {
				field: "country_code",
				title: "Code",
				width: 100,
				responsive: {visible: 'lg'}
			}, {
				field: "phone",
				title: "Phone",
				responsive: {visible: 'lg'}
			}, {
				field: "role",
				title: "Role",
				responsive: {visible: 'lg'}
			},{
				field: "last_login",
				title: "Last Login"
			},  {
				field: "is_login",
				title: "Is Login",
				width: 100,
				// callback function support for column rendering
				template: function (row) {
					var status = {
						0: {'title': 'Logout', 'state': 'accent'},
						1: {'title': 'Login', 'state': 'danger'}
					};
					return '<span class="m-badge m-badge--' + status[row.is_login].state + ' m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-' + status[row.is_login].state +'">' + status[row.is_login].title + '</span>';
					
				}
			}, {
				field: "is_active",
				title: "Is Active",
				width: 100,
				// callback function support for column rendering
				template: function (row) {
					var status = {
						0: {'title': 'Inactive', 'class': 'm-badge--brand'},
						1: {'title': 'Active', 'class': 'm-badge--success'}
					};
					return '<span class="m-badge ' + status[row.is_active].class + ' m-badge--wide">' + status[row.is_active].title + '</span>';
				}
			}, {
				field: "Actions",
				width: 110,
				title: "Actions",
				sortable: false,
				overflow: 'visible',
				template: function (row, index, datatable) {
					var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';

					return '\
						<a href="view/'+row.id+'" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill">\
							<i class="la la-eye"></i>\
						</a>\
						<a href="edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
							<i class="la la-edit"></i>\
						</a>\
						<a href="remove/'+row.id+'" onclick="return confirm(\'Are you sure you want to delete staff ?\');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\
							<i class="la la-trash"></i>\
						</a>\
					';
				}
			}]
		});
	};

	return {
		// public functions
		init: function () {
			demo();
		}
	};
}();


jQuery(document).ready(function () {
	DefaultDatatableDemo.init();
});