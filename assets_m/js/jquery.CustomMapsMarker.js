PulsationMarker.prototype = new google.maps.OverlayView();

function PulsationMarker(opts) {
    this.setValues(opts);
}

PulsationMarker.prototype.draw = function() {
    var self = this;
    var div = this.div;

    if (!div) {
        div = this.div = $('' +
            '<div>' +
            '<div class="shadow"></div>' +
            '<div class="pulse ' + this.state + '"></div>' +
            '<div class="pin-label">' + this.title +
            '</div>' +
            '</div>' +
            '')[0];
        this.pinWrap = this.div.getElementsByClassName('pin-label');
        this.pin = this.div.getElementsByClassName('pin');
        div.style.position = 'absolute';
        div.style.cursor = 'pointer';
        div.class = this.state;
        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);
        google.maps.event.addDomListener(div, "click", function(event) {
            google.maps.event.trigger(self, "click", event);
        });
    }
    var point = this.getProjection().fromLatLngToDivPixel(this.position);
    if (point) {
        div.style.left = point.x + 'px';
        div.style.top = point.y + 'px';
    }
};

PulsationMarker.prototype.delete = function () {
    this.div.parentNode.removeChild(this.div);
};
