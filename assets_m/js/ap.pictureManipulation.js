/**
 *
 */
var PictureManipulation = function () {
    /**
     *
     * @type {string}
     */
    this.parentElementClassName = 'body';

    /**
     *
     * @type {string}
     */
    this.manipulationTriggerName = '.js-image-manipulation-trigger';

    /**
     *
     * @type {*}
     */
    this.$saveManipulatedImageButton = $('.js-save-manipulated-image');

    /**
     *
     * @type {*}
     */
    this.$closeManipulationOverlayButton = $('.js-close-image-manipulation-overlay');

    /**
     *
     * @type {*}
     */
    this.imageEditorContainerClassName = 'js-image-editor-container';

    /**
     *
     * @type {*}
     */
    this.$actionButtons = $('.js-image-manipulation-toolbar').find('.js-image-manipulation-action-button');

    /**
     *
     * @type {*}
     */
    this.overlayClassName = '.js-overlay';

    /**
     *
     * @type {null}
     */
    this.imageEditor = null;

    /**
     *
     * @type {null}
     */
    this.$sourceImageElem = null;

    /**
     *
     * @type {string}
     */
    this.targetImageFileExtension = 'jpg';

    /**
     *
     * @type {string}
     */
    this.sourceImageDirectory = null;

    /**
     *
     * @type {string}
     */
    this.imageName = null;

    /**
     *
     * @type {*}
     */
    this.$feedbackSuccessElem = $('.js-feedback-success');

    /**
     *
     * @type {*}
     */
    this.$feedbackErrorElem = $('.js-feedback-error');

    /**
     *
     * @type {*}
     */
    this.$resourceElem = $('.js-image-resource-id');

    /**
     * E.g. a driver ID or user ID. The resource ID is included in the request
     *
     * @type {number}
     */
    this.resourceId = this.$resourceElem.data('resource-id') || null;

    /**
     * Type of resource we edit, e.g. a "driver" or "user"
     * @type {*}
     */
    this.resourceType = this.$resourceElem.data('resource-type') || null;

    /**
     * The database field the resource belongs to. E.g. "profile_picture"
     * @type {null}
     */
    this.resourceName = null;

    this.bindImageEditorTriggerClickEvent();
};

/**
 *
 */
PictureManipulation.prototype.bindImageEditorTriggerClickEvent = function () {
    $(this.manipulationTriggerName).on('click', PictureManipulation.prototype.initImageEditor.bind(this));
};

/**
 * Init the image editor and show the page overlay
 *
 * @param e
 */
PictureManipulation.prototype.initImageEditor = function (e) {
    this.$sourceImageElem = $(e.currentTarget);
    var $mainPageElem = this.$sourceImageElem.parents().find(this.parentElementClassName);
    var $overlay = $mainPageElem.find(this.overlayClassName);
    var fullSizePicturePath = this.$sourceImageElem.attr('data-full-image-src'); // use attr() instead of data() otherwise old image might be loaded when editing the same image more than 1 time
    this.imageName = this.$sourceImageElem.data('image-name').split(".")[0] + "." + this.targetImageFileExtension || +new Date() + "." + this.targetImageFileExtension;
    this.sourceImageDirectory = this.$sourceImageElem.attr('data-source-image-directory');
    this.resourceName = this.$sourceImageElem.data('resource-name') || null;

    $mainPageElem.addClass('no-scroll');

    if (typeof fullSizePicturePath === 'undefined' || fullSizePicturePath.length === 0) {
        throw 'Image ' + fullSizePicturePath + '  does not exist';
    }

    // Get page width
    $overlay.show();
    var editableImageContainerWidth = $mainPageElem.width() * 0.9;
    var $editableImage = this.appendImageEditorToOverlay($overlay, fullSizePicturePath, editableImageContainerWidth);

    this.imageEditor = this.createImageEditorInstance($editableImage);
};

/**
 *
 * @param $overlay
 * @param imagePath
 * @param width
 * @returns {*|jQuery|HTMLElement}
 */
PictureManipulation.prototype.appendImageEditorToOverlay = function ($overlay, imagePath, width) {
    $overlay.find('.js-relative-overlay-container')
        .append("<div class='"+this.imageEditorContainerClassName+" image-editor-container'><img class='js-editable-image' src='" + imagePath + "' /></div>");
    $('.image-editor-container').css('width', width);

    return $('.js-editable-image');
};

/**
 *
 * @param $editableImageElement
 * @returns {*}
 */
PictureManipulation.prototype.createImageEditorInstance = function ($editableImageElement) {
    var cropper = new Cropper($editableImageElement[0], {
        checkOrientation: false, // When true, it dramatically increases file size see https://github.com/fengyuanchen/cropper/issues/542
        autoCrop: false
    });

    // Attach the save and close event
    this.$saveManipulatedImageButton.on('click', PictureManipulation.prototype.saveImage.bind(this));
    this.$closeManipulationOverlayButton.on('click', PictureManipulation.prototype.closeImageEditor.bind(this));

    // Initialize the toolbar action buttons
    this.initializeToolbarActionButtons(cropper);

    return cropper;
};

/**
 * Attach click events to the toolbar action buttons.
 */
PictureManipulation.prototype.initializeToolbarActionButtons = function (cropper) {
    this.$actionButtons.on('click', (function (event) {
        var e = event || window.event;
        var target = e.target || e.srcElement;
        var result;
        var input;
        var data;

        if (!cropper) {
            return;
        }

        while (target !== this) {
            if (target.getAttribute('data-method')) {
                break;
            }

            target = target.parentNode;
        }

        if (target.disabled || target.className.indexOf('disabled') > -1) {
            return;
        }

        data = {
            method: target.getAttribute('data-method'),
            target: target.getAttribute('data-target'),
            option: target.getAttribute('data-option'),
            secondOption: target.getAttribute('data-second-option')
        };

        if (data.method) {
            if (typeof data.target !== 'undefined') {
                input = document.querySelector(data.target);

                if (!target.hasAttribute('data-option') && data.target && input) {
                    try {
                        data.option = JSON.parse(input.value);
                    } catch (e) {
                        throw e.message;
                    }
                }
            }

            switch (data.method) {
                case 'rotate':
                    cropper.clear();
                    break;

                case 'getCroppedCanvas':
                    try {
                        data.option = JSON.parse(data.option);
                    } catch (e) {
                        throw e.message;
                    }

                    if (uploadedImageType === 'image/jpeg') {
                        if (!data.option) {
                            data.option = {};
                        }

                        data.option.fillColor = '#fff';
                    }

                    break;
            }

            result = cropper[data.method](data.option, data.secondOption);

            switch (data.method) {
                case 'rotate':
                    cropper.crop();
                    break;

                case 'scaleX':
                case 'scaleY':
                    target.setAttribute('data-option', -data.option);
                    break;

                case 'getCroppedCanvas':
                    if (result) {
                        // Bootstrap's Modal
                        $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

                        if (!download.disabled) {
                            download.href = result.toDataURL(uploadedImageType);
                        }
                    }

                    break;

                case 'destroy':
                    cropper = null;
                    break;
            }

            if (typeof result === 'object' && result !== cropper && input) {
                try {
                    input.value = JSON.stringify(result);
                } catch (e) {
                    throw e.message;
                }
            }
        }
    }).bind(this));
};

/**
 * Saves an images. If the browser supports toBlob(), the altered images is uploaded to the server. Otherwise,
 * we convert the image to a data url.
 */
PictureManipulation.prototype.saveImage = function () {
    if (!HTMLCanvasElement.prototype.toBlob) {
        // ToDo: this.imageEditor.cropper('getCroppedCanvas').toDataURL('image/jpeg');
        throw 'Browser does not support toBlob()';
    }

    this.saveImageAsBlob();
};

/**
 * Uploads the modified image as a blob file to the server.
 */
PictureManipulation.prototype.saveImageAsBlob = function () {
    this.imageEditor.getCroppedCanvas({
        imageSmoothingEnabled: true,
        imageSmoothingQuality: 'high'
    });

    // Make the variables accessible in the "toBlobl()" method
    var newImageName = this.imageName; // Could have been changed because of the original extensions. All modified images are saved as jpg
    var sourceImageName = this.imageName;
    var sourceImageDirectory = this.sourceImageDirectory;

    var scope = this; // Ugly workaround to pass current scope.

    // Upload cropped image to server if the browser supports `HTMLCanvasElement.toBlob`
    this.imageEditor.getCroppedCanvas().toBlob(function (blob) {
        var formData = new FormData();

        formData.append('resourceId', scope.resourceId);
        formData.append('resourceType', scope.resourceType);
        formData.append('resourceName', scope.resourceName);
        formData.append('croppedImage', blob, newImageName);
        formData.append('newImageName', newImageName);
        formData.append('sourceImageName', newImageName);
        formData.append('sourceImageDirectory', sourceImageDirectory);

        // Use `jQuery.ajax` method
        $.ajax('/business/ImageManipulation/saveBlob', {
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function(response) {
                var message = response.text;

                scope.$feedbackSuccessElem.text(message).removeClass('hidden').delay(10000).queue(function () {
                    $(this).addClass('hidden');
                });

                scope.alterSourceImageElement(sourceImageDirectory, response.imageName, sourceImageDirectory);
                scope.closeImageEditor();
            },
            error: function(xhr, status, error) {
                var message = xhr.responseJSON.text;

                scope.$feedbackErrorElem.text(message).removeClass('hidden').delay(10000).queue(function () {
                    $(this).addClass('hidden');
                });

                scope.closeImageEditor();
            }
        });
    }, 'image/jpeg', 5);
};

/**
 *
 * @param {String} directory
 * @param {String} imageName
 * @param {String} sourceImageDirectory
 */
PictureManipulation.prototype.alterSourceImageElement = function(directory, imageName, sourceImageDirectory)
{
    if(!this.$sourceImageElem) {
        throw 'Cannot alter source image element, source is null';
    }

    // Alter thumbnail
    var originalSourceThumbPath = this.$sourceImageElem.attr('src');
    var originalSourceThumb = originalSourceThumbPath.substring(0, originalSourceThumbPath.lastIndexOf("/") + 1);
    var newThumbSourcePath = originalSourceThumb + imageName;

    this.$sourceImageElem.attr('src', newThumbSourcePath);

    // Alter data image source
    var originalSourceFull = sourceImageDirectory.substring(0, sourceImageDirectory.lastIndexOf("/") + 1);
    var newFullSource = originalSourceFull + imageName;

    this.$sourceImageElem.attr('data-full-image-src', newFullSource);
    this.$sourceImageElem.attr('data-image-name', imageName);
};

/**
 *
 * @param e
 */
PictureManipulation.prototype.hideOverlay = function (e) {
    var $overlay = $(this.overlayClassName);
    $overlay.unbind('click');
    $overlay.find('.' + this.imageEditorContainerClassName).remove();
    $overlay.hide();
};

/**
 * Destroy the image editor, hide page overlay and unbind events
 */
PictureManipulation.prototype.closeImageEditor = function () {
    this.hideOverlay();

    var $mainPageElem = $(this.parentElementClassName);
    $mainPageElem.removeClass('no-scroll');

    if(this.imageEditor !== null) {
        this.imageEditor.destroy();
        this.imageEditor = null;
    }

    this.sourceImageDirectory = null;
    this.imageName = null;
    this.$sourceImageElem = null;

    this.$actionButtons.unbind('click');
    this.$saveManipulatedImageButton.unbind('click');
    this.$closeManipulationOverlayButton.unbind('click');
};

new PictureManipulation();
