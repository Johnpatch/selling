(function ($) {

    var sticky = false;

    function stickyNav() {
        var $headerNav = $('.header-nav--container');
        var $window = $(window);

        if ($window.scrollTop() >= 40) {
            if (!sticky) {
                $headerNav.addClass('sticky');
                sticky = true;
            }
        }
        else {
            $headerNav.removeClass('sticky');
            sticky = false;
        }

        var $before = $('.header-nav__bg');

        var opacity = ($window.scrollTop() / $headerNav.height() * 100) / 100;
        if (opacity >= 0) {
            $before.css('opacity', (opacity));
        }
    }


    jQuery(document).ready(function ($) {

        stickyNav();

        // activate sticky menu
        $(window).on('scroll', function () {
            stickyNav();
        });

        // smooth scroll on nav link click
        $('.main-navigation li.menu-item-type-custom a').on('click', function (e) {
            e.preventDefault();

            $('.is-active').removeClass('is-active');

            anchor = $(this).attr('href');

            if ($('body.home').length) {

                $('html, body').animate({
                    scrollTop: $(anchor).offset().top - 150
                }, 1000);
            }
            else {
                window.location.href = '/' + anchor;
            }

        });

    });


})(jQuery);