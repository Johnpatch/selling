var map;
var directionsService;
var directionsDisplay;

/**
 *
 */
function intialiseActivityMap() {

    directionServiceActiveForDriverId = null;
    trips = {};
    drivers = {};
    this.mapActivitySidebar = document.getElementById('leftMapActivitySidebar');
    this.locationsList = this.mapActivitySidebar.getElementsByClassName('js-locations-list')[0];
    isSmallScreen = document.getElementsByClassName('smallscreen')[0] || false;

    this.defaultLocation = this.locationsList.getElementsByClassName('js-location')[0]

    if(this.locationsList.getElementsByClassName('js-default').length > 0) {
      this.defaultLocation = this.locationsList.getElementsByClassName('js-default')[0];
    }

    this.selectedStateOrTerritory = this.defaultLocation.getAttribute('data-state');

    initMap();
    initialiseChannels();
    initEventListeners();
    loadCurrentActivities();
}

function initEventListeners() {
    var locations = locationsList.getElementsByClassName('js-location');

    for(var i = 0; i < locations.length; i++) {
        locations[i].addEventListener('click', function (e) {
            var elem = e.currentTarget;

            var activeElem = locationsList.getElementsByClassName('js-active')[0];
            activeElem.classList.remove('js-active', 'active');

            elem.classList.add('js-active', 'active');

            map.panTo(
                new google.maps.LatLng(
                    elem.getAttribute('data-lat'),
                    elem.getAttribute('data-lng')
                )
            );
            map.setZoom(13);
            selectedStateOrTerritory = elem.getAttribute('data-state');
            initialiseChannels();
            loadCurrentActivities();
        });
    }
}

/**
 * Loads current drivers and trip data on the map.
 */
function loadCurrentActivities() {
    $.ajax({
        type: "GET",
        url: BASE_URL + "/business/ActivityMap/loadData/" + selectedStateOrTerritory,
        success: populateMapWithCurrentActivities,
        failure: function (a,b,c) {
            console.error("Request failed", a, b,c)
        }
    });
}

/**
 *
 * @param data
 */
function populateMapWithCurrentActivities(data) {
    for(var d = 0; d < data.drivers.length; d++) {
        var driverData = data.drivers[d];
        drivers[driverData.driver.id] = getOrCreateNewDriverMarker(driverData.driver);

        if(parseInt(driverData.driver.is_free) === 0) {
            changeDriverIconToNotFree(drivers[driverData.driver.id]);

            if (driverData.trip) {
                switch (driverData.trip.status) {
                    case "Assigned":
                        addDisplayRouteClickEventToMarker(
                            drivers[driverData.driver.id],
                            new google.maps.LatLng(
                                driverData.driver.lat,
                                driverData.driver.lng
                            ),
                            new google.maps.LatLng(
                                driverData.trip.pickup.lat,
                                driverData.trip.pickup.lng
                            )
                        );
                        break;

                    case "Arrived":
                        // Do nothing
                        break;

                    case "Processing":
                        addDisplayRouteClickEventToMarker(
                            drivers[driverData.driver.id],
                            new google.maps.LatLng(
                                driverData.trip.pickup.lat,
                                driverData.trip.pickup.lng
                            ),
                            new google.maps.LatLng(
                                driverData.trip.dropoff.lat,
                                driverData.trip.dropoff.lng
                            )
                        );
                        break;
                }
            }
        }
    }

    for(var t = 0; t < data.trips.length; t++) {
        var tripData = data.trips[t];

        switch(tripData.trip.status) {
            case "Waiting":
                // Ride later is not displayed when the current activities are loaded
                addNewTripRequest(tripData);
                break;

            case "Assigned":
                tripAssigned(tripData);
                break;

            case "Arrived":
                driverArrived(tripData);
                break;

            default:
                console.error('Unsupported trip', tripData);
        }
    }
}

/**
 *
 */
function initialiseChannels() {
  socket.emit('joinRoom', this.selectedStateOrTerritory);

  // Trip
  socket.on('new-trip-request', addNewTripRequest);
  socket.on('new-trip-later-request', showNewTripLaterRequest);
  socket.on('driver-accepts-ride-request-assigned', tripAssigned);
  socket.on('passengers-dropped-off', tripDropOff);
  socket.on('passengers-picked-up', passengersPickedUp);
  socket.on('trip-cancelled', tripCancelled);

  // Fare estimate
  socket.on('new-fare-estimate', newFareEstimate);

  // Rider
  socket.on('rider-map-screen-open', riderMapScreenOpen);

  // Driver
  socket.on('driver-turned-service-on', driverTurnedServiceOn);
  socket.on('driver-turned-service-off', driverTurnedServiceOff);
  socket.on('driver-location-update', driverLocationUpdate);
  socket.on('driver-arrived', driverArrived);

}

/**
 *
 * @param data
 */
function showNewTripLaterRequest(data) {
    if (typeof trips[data.trip.id] !== 'undefined') {
        console.error('[showNewTripLaterRequest] Trip ' + data.trip.id + ' marker already exists');
        return;
    }

    var title = "New Ride Later #" + data.trip.id;
    var tempMarker = createPulsationMarker(map, 'new-trip-later', title, data.trip.pickup.lat, data.trip.pickup.lng);

    removePulsationMarkerFromMapAfterTimeout(tempMarker, 8000)
}

/**
 *
 * @param data
 */
function addNewTripRequest(data) {
    resetTripData(data);

    var title = "New Request #" + data.trip.id;
    trips[data.trip.id] = createPulsationMarker(map, 'new-trip', title, data.trip.pickup.lat, data.trip.pickup.lng);
}

/**
 *
 * @param data
 */
function tripCancelled(data) {
    resetTripData(data);

    drivers[data.driver.id] = getOrCreateNewDriverMarker(data.driver);
    clearDriverMarkerClickEvent(data.driver.id);
    changeDriverIconToFree(drivers[data.driver.id]);
    hideDirectionsDisplay(data.driver.id);

    var tempMarker = createPulsationMarker(map, 'cancel', data.message, data.trip.pickup.lat, data.trip.pickup.lng);
    removePulsationMarkerFromMapAfterTimeout(tempMarker, 8000);
}

/**
 *
 * @param data
 */
function tripAssigned(data) {
    resetTripData(data);

    var title = "Waiting for driver #" + data.driver.id + " arrival";
    trips[data.trip.id] = createPulsationMarker(map, 'waiting-for-driver', title, data.trip.pickup.lat, data.trip.pickup.lng);

    // Update driver object or insert new driver
    drivers[data.driver.id] = getOrCreateNewDriverMarker(data.driver);

    addDisplayRouteClickEventToMarker(
        drivers[data.driver.id],
        drivers[data.driver.id].getPosition(),
        new google.maps.LatLng(
            data.trip.pickup.lat,
            data.trip.pickup.lng
        )
    );
    drivers[data.driver.id]._isClickEventBound = true;

    changeDriverIconToNotFree(drivers[data.driver.id]);
}

/**
 *
 * @param data
 */
function tripDropOff(data) {
    var title = "Passengers dropped off. Trip " + data.trip.id + ". $" + data.trip.fare;
    var tempMarker = createPulsationMarker(map, 'dropoff', title, data.trip.dropoff.lat, data.trip.dropoff.lng);

    removePulsationMarkerFromMapAfterTimeout(tempMarker, 10000);

    drivers[data.driver.id] = getOrCreateNewDriverMarker(data.driver);

    // Update driver position
    drivers[data.driver.id].setPosition(
        new google.maps.LatLng(data.trip.dropoff.lat, data.trip.dropoff.lng)
    );

    clearDriverMarkerClickEvent(data.driver.id);
    hideDirectionsDisplay(data.driver.id);

    changeDriverIconToFree(drivers[data.driver.id]);
}

/**
 *
 * @param data
 */
function passengersPickedUp(data) {
    resetTripData(data);

    drivers[data.driver.id] = getOrCreateNewDriverMarker(data.driver);

    addDisplayRouteClickEventToMarker(
        drivers[data.driver.id],
        new google.maps.LatLng(
            data.trip.pickup.lat,
            data.trip.pickup.lng
        ),
        new google.maps.LatLng(
            data.trip.dropoff.lat,
            data.trip.dropoff.lng
        )
    );
    drivers[data.driver.id]._isClickEventBound = true;
}

/**
 *
 * @param data
 */
function driverArrived(data) {
    resetTripData(data);

    trips[data.trip.id] = createPulsationMarker(map, 'driver-arrived', 'Driver arrived', data.trip.pickup.lat, data.trip.pickup.lng);

    if (typeof drivers[data.driver.id] !== 'undefined') {
        // Remove click event listener
        clearDriverMarkerClickEvent(data.driver.id);
    } else {
        drivers[data.driver.id] = getOrCreateNewDriverMarker(data.driver);
    }

    // Update driver position
    drivers[data.driver.id].setPosition(
        new google.maps.LatLng(data.trip.pickup.lat, data.trip.pickup.lng)
    );

    hideDirectionsDisplay(data.driver.id);
}

/**
 *
 * @param data
 */
function resetTripData(data) {
    if (typeof trips[data.trip.id] !== 'undefined') {
        trips[data.trip.id].delete();
        delete trips[data.trip.id];
    }
}

/**
 *
 * @param data
 */
function driverTurnedServiceOn(data) {
    drivers[data.driver.id] = getOrCreateNewDriverMarker(data.driver, true);
}

/**
 *
 * @param data
 */
function driverLocationUpdate(data) {
    drivers[data.driver.id] = getOrCreateNewDriverMarker(data.driver);

    var numDeltas = 50;
    var delay = 10; //milliseconds
    var i = 0;
    var deltaLat;
    var deltaLng;
    var position = [
        drivers[data.driver.id].getPosition().lat(),
        drivers[data.driver.id].getPosition().lng()
    ];
    deltaLat = (data.driver.lat - position[0]) / numDeltas;
    deltaLng = (data.driver.lng - position[1]) / numDeltas;
    moveMarker();

    function moveMarker() {
        position[0] += deltaLat;
        position[1] += deltaLng;
        var latlng = new google.maps.LatLng(position[0], position[1]);
        drivers[data.driver.id].setPosition(latlng);
        if (i != numDeltas) {
            i++;
            setTimeout(moveMarker, delay);
        }
    }
}

/**
 *
 * @param data
 */
function riderMapScreenOpen(data) {
    console.log(data, data.user)
    var marker = new google.maps.Circle({
        strokeOpacity: 0,
        fillColor: '#6ffffc',
        fillOpacity: 0.65,
        map: map,
        center: new google.maps.LatLng(data.user.lat, data.user.lng),
        radius: 100
    });

    setInterval(function () {
        marker.set("fillOpacity", marker.get("fillOpacity") - 0.02);
    }, 100);

    removeMarkerFromMapAfterTimeout(marker, 15000);
}


/**
 *
 * @param data
 */
function driverTurnedServiceOff(data) {
    if (typeof drivers[data.driver.id] !== 'undefined') {
        removeMarkerFromMap(drivers[data.driver.id]);

        delete drivers[data.driver.id];
    }
}

/**
 *
 * @param data
 */
function newFareEstimate(data) {
    var title = "New fare estimate";
    if(isAdmin === 'true') {
        title += " $" + data.estimate.fare;
    }
    var fareEstimatePulsationMarker = createPulsationMarker(map, 'fareestimate', title, data.estimate.pickup.lat, data.estimate.pickup.lng);

    removePulsationMarkerFromMapAfterTimeout(fareEstimatePulsationMarker, 8000);
}

/**
 *
 * @param map
 * @param state
 * @param title
 * @param lat
 * @param lng
 * @returns {PulsationMarker}
 */
function createPulsationMarker(map, state, title, lat, lng) {
    return new PulsationMarker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        state: state,
        title: title
    });
}

/**
 *
 * @param marker
 */
function removeMarkerFromMap(marker) {
    marker.setMap(null);
}

/**
 *
 */
function clearDirectionsDisplay() {
    if(directionsDisplay !== null) {
        directionsDisplay.setMap(null);
    }
}

function removeMarkerFromMapAfterTimeout(marker, timeOut) {
    setTimeout(function () {
        marker.setMap(null);
        marker = null;
    }, timeOut);
}

/**
 *
 * @param pulsationMarker
 * @param timeOut
 */
function removePulsationMarkerFromMapAfterTimeout(pulsationMarker, timeOut) {
    setTimeout(function () {
        pulsationMarker.delete();
        pulsationMarker = null;
    }, timeOut);
}

/**
 *
 * @param marker
 * @param pointA
 * @param pointB
 */
function addDisplayRouteClickEventToMarker (marker, pointA, pointB) {
    if(isAdmin === 'true') {
        marker.addListener('click', function () {
            return calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB, marker._driverId);
        });
    }
}

/**
 *
 * @param markerId
 */
function clearDriverMarkerClickEvent(markerId) {
    if(drivers[markerId]._isClickEventBound) {
        google.maps.event.clearListeners(drivers[markerId], 'click');
        drivers[markerId]._isClickEventBound = false;
    }
}

/**
 *
 * @param driverId
 */
function hideDirectionsDisplay(driverId) {
    if(directionServiceActiveForDriverId === parseInt(driverId)) {
        directionsDisplay.setMap(null);
        directionServiceActiveForDriverId = null;
    }
}

/**
 *
 * @param data
 * @returns {google.maps.Marker}
 */
function getOrCreateNewDriverMarker(data, animate)
{
    animate = animate || false;
    if (typeof drivers[data.id] !== 'undefined') {
        return drivers[data.id];
    }

    var icon;
    switch(data.type) {
        case "laoxi":
        case "Laoxi":
            icon = "laoxi";
            break;

        case "big laoxi":
        case "Big Laoxi":
        case "biglaoxi":
        case "big_laoxi":
            icon = "biglaoxi";
            break;

        case "fancy laoxi":
        case "Fancy Laoxi":
        case "fancylaoxi":
        case "fancy_laoxi":
            icon = "fancylaoxi";
            break;

        default:
            icon = "laoxi";
    }

    if(data.gender && data.gender === 'female') {
        icon += '_female';
    }

    var marker =  new google.maps.Marker({
        position: new google.maps.LatLng(data.lat, data.lng),
        map: map,
        label: {
            text: isAdmin === 'true' ? "Driver #" + data.id : 'Driver',
            color: 'white',
            fontSize: '12px'
        },
        animation: animate ? google.maps.Animation.DROP : false,
        icon: {
            url: BASE_URL + 'assets/images/cars/' + icon + '.png',
            labelOrigin: new google.maps.Point(50, 30)
        }
    });

    marker._driverId = data.id;

    return marker;
}

/**
 *
 * @param directionsService
 * @param directionsDisplay
 * @param pointA
 * @param pointB
 * @param driverId
 */
function calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB, driverId) {
    if(parseInt(driverId) === directionServiceActiveForDriverId && directionsDisplay.getMap()) {
        hideDirectionsDisplay(driverId);
        return;
    }
    directionServiceActiveForDriverId = parseInt(driverId); // Set value so we can close directions automatically when desired
    directionsService.route({
        origin: pointA,
        destination: pointB,
        travelMode: google.maps.TravelMode.DRIVING
    }, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
    directionsDisplay.setMap(map);
}

/**
 *
 * @param marker
 */
function changeDriverIconToFree(marker) {
    var markerUri = marker.getIcon().url;
    var strippedIconUri = markerUri.replace(/\.[^/.]+$/, "");

    if(strippedIconUri.indexOf('_not_free') === 0) {
        return;
    }

    var newIcon = markerUri.replace(/_not_free/i, '');

    marker.setIcon({
        url: newIcon,
        labelOrigin: marker.getIcon().labelOrigin
    });
}

/**
 *
 * @param marker
 */
function changeDriverIconToNotFree(marker) {
    var markerUri = marker.getIcon().url;
    var strippedIconUri = markerUri.replace(/\.[^/.]+$/, "");

    if(strippedIconUri.indexOf('_not_free') > -1) {
        return;
    }

    var newIcon = strippedIconUri + "_not_free.png";

    marker.setIcon({
        url: newIcon,
        labelOrigin: marker.getIcon().labelOrigin
    });
}

// Google Maps init
function initMap() {
    map = new google.maps.Map(document.getElementById('activity-map'), {
        zoom: 4,
        streetViewControl: false,
        mapTypeControl: false,
        styles: [ // Dark theme
            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            {
                featureType: 'administrative.locality',
                elementType: 'labels.text.fill',
                stylers: [{color: '#d59563'}]
            },
            {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{color: '#d59563'}]
            },
            {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [{color: '#263c3f'}]
            },
            {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{color: '#6b9a76'}]
            },
            {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{color: '#38414e'}]
            },
            {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [{color: '#212a37'}]
            },
            {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{color: '#9ca5b3'}]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{color: '#746855'}]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{color: '#1f2835'}]
            },
            {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [{color: '#f3d19c'}]
            },
            {
                featureType: 'transit',
                elementType: 'geometry',
                stylers: [{color: '#2f3948'}]
            },
            {
                featureType: 'transit.station',
                elementType: 'labels.text.fill',
                stylers: [{color: '#d59563'}]
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [{color: '#17263c'}]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{color: '#515c6d'}]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#17263c'}]
            }
        ]
    });

    if(isSmallScreen) {
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(this.mapActivitySidebar);
    }

    map.setCenter(new google.maps.LatLng(this.defaultLocation.getAttribute('data-lat'), this.defaultLocation.getAttribute('data-lng')));
    map.setZoom(14);

    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer({
        map: this.map
    });
}

intialiseActivityMap();