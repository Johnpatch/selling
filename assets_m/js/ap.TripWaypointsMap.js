var map;

/**
 *
 * @constructor
 */
var TripWaypointsMap = function () {


    this.map = map;

    /**
     *
     * @type {string}
     */
    this.mapElemntSelector = '.js-trip-waypoints';

    /**
     *
     * @type {jQuery|HTMLElement}
     */
    this.$mapElement = $(this.mapElemntSelector);

    /**
     *
     * @type {null|integer}
     */
    this.tripId = null;

    /**
     *
     * @type {null|string}
     */
    this.requestUri = null;

    /**
     *
     * @type {Array}
     */
    this.markers = [];

    /**
     *
     * @type {Array}
     */
    this.polylinePoints = [];

    /**
     *
     * @type {Array}
     */
    this.addedWaypoints = [];

    this.tripCompleted = null;

    /**
     *
     * @type {string}
     */
    this.apiKey = '';

    this.initialize();
};

/**
 *
 */
TripWaypointsMap.prototype.initialize = function () {
    this.tripId = this.$mapElement.data('trip-id');
    this.requestUri = this.$mapElement.data('trip-waypoint-request-uri') +'/'+this.tripId;
    this.apiKey = this.$mapElement.data('maps-api-key');
    this.tripCompleted = this.$mapElement.data('trip-completed');

    this.requestData();
};

/**
 *
 */
TripWaypointsMap.prototype.requestData = function () {
    var scope = this; // Ugly workaround to pass current scope.

    $.ajax({
        type: "GET",
        dataType: 'json',
        url: this.requestUri,
        success: function (results) {
            scope.handleSuccessResponse(results)
        },
        error: function (a, b, c) {
            console.log("error occurred:", a,b,c)
        }
    });
};

/**
 *
 * @param waypoints
 */
TripWaypointsMap.prototype.handleSuccessResponse = function (waypoints) {
    this.removePolylinePointsAndClearMap();

    this.addWaypointsToMap(waypoints);
    this.drawSnappedPolyline(waypoints);
};

/**
 *
 * @param {Object} waypoints
 * @returns {string}
 */
TripWaypointsMap.prototype.waypointsToPipeSeparatedString = function (waypoints) {
    var waypointsArray = [];

    for (var i = 0; i < waypoints.length; i++) {
        var coordinates = waypoints[i];
        var lat;
        var lng;

        if(coordinates instanceof google.maps.LatLng) {
            lat = coordinates.lat();
            lng = coordinates.lng();
        } else {
            lat = coordinates['latitude'];
            lng = coordinates['longitude'];
        }

        var location = parseFloat(lat) +','+parseFloat(lng);

        waypointsArray.push(location);
    }

    return waypointsArray.join('|');
};

/**
 *
 * @param waypoints
 */
TripWaypointsMap.prototype.addWaypointsToMap = function (waypoints) {
    var length = waypoints.length;
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < length; i++) {
        var latlng = new google.maps.LatLng(
            parseFloat(waypoints[i]['latitude']),
            parseFloat(waypoints[i]['longitude'])
        );
        this.addedWaypoints.push(latlng);
        bounds.extend(latlng);
    }

    this.map.fitBounds(bounds);
};

/**
 *
 * @param waypoints
 */
TripWaypointsMap.prototype.drawSnappedPolyline = function (waypoints) {
    var snappedPolyline = new google.maps.Polyline({
        path: this.addedWaypoints
    });

    for (var i = 0; i < waypoints.length; i++) {
        var pointCount = 1 + i;
        var point = waypoints[i];
        var options = {
            position: {
                'lat': parseFloat(point['latitude']),
                'lng': parseFloat(point['longitude'])
            },
            map: this.map
        };

        if(i === 0 || i === waypoints.length - 1) {
            options['icon'] = 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+pointCount+'|4286f4|000000';
            options['animation'] = google.maps.Animation.BOUNCE;
        }

        var marker = new google.maps.Marker(options);
        this.markers.push(marker);
    }

    snappedPolyline.setMap(this.map);
    this.polylinePoints.push(snappedPolyline);
};

/**
 *
 */
TripWaypointsMap.prototype.removePolylinePointsAndClearMap = function () {
    for (var i = 0; i < this.polylinePoints.length; i++) {
        this.polylinePoints[i].setMap(null);
    }

    this.polylinePoints = [];
};

/**
 * Blegh, ugly init.
 */
function initMap() {
    var $elem = $('.js-waypoints-tab');
    var isInitalised = $elem.attr('data-initialised');

    if(isInitalised === 'true') { // Prevent from reloading the map again
        return;
    }

    $elem.attr('data-initialised', 'true');

    map = new google.maps.Map(document.getElementById('js-trip-waypoints-map'), {
        zoom: 4
    });

    new TripWaypointsMap();
}

// Ugly fix for using maps inside Twitter Bootstrap tab. See: https://github.com/twbs/bootstrap/issues/2330
$('a[href="#waypoints"]').on('shown.bs.tab', function (e) {
    initMap();
}) ;