$(document).ready(function () {
    var $nextButton = $('.js-button-next');
    var $loginButton = $('.js-button-login');
    var $otpFormGroup = $('.js-otp-group');
    var $otpInput = $otpFormGroup.find('.js-otp-input');
    var $phoneFormGroup = $('.js-phone-number-group');
    var $phoneInput = $phoneFormGroup.find('.js-phonenumber-input');
    var $jsAlert = $('.js-alert');
    var url;
    var data = {};

    $nextButton.on('click', function (e) {
        url = $phoneFormGroup.attr('data-url');
        data = 'phonenumber='+$phoneInput.val();

        postData(url, data, showOTP);

    });

    $loginButton.on('click', function (e) {
        url = $otpFormGroup.attr('data-url');
        data = 'otp='+$otpInput.val();

        postData(url, data, redirect);

    });

    function showOTP() {
        $phoneFormGroup.addClass('hidden').queue(function () {
            $otpFormGroup.removeClass('hidden');
        });
    }

    function reset() {
        $otpFormGroup.addClass('hidden').queue(function () {
            $phoneFormGroup.removeClass('hidden');
        });

        $phoneInput.val('');
        $otpInput.val('');
    }

    function postData(url, data, callback) {
        $.ajax({
            cache: false,
            url: url,
            type: "POST",
            data: data,
            success: function (response) {
                if(response['success'] === true) {
                    if(typeof response['link'] !== 'undefined') {
                        callback(response['link']);
                    } else {
                        callback();
                    }

                } else {
                    showError(response['error']);
                }
            },
            error: function (xhr, status, error) {
                showError(JSON.parse(xhr.responseText));
            }
        });
    }

    function redirect(url) {
        window.location = url;
    }

    function showError(error) {
        $jsAlert.removeClass('hidden').text(error).delay(8000).queue(function () {
            $jsAlert.text('').addClass('hidden');
        }.bind(this));
    }
});