/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100116
 Source Host           : localhost:3306
 Source Schema         : selling

 Target Server Type    : MySQL
 Target Server Version : 100116
 File Encoding         : 65001

 Date: 15/10/2019 01:43:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_lang
-- ----------------------------
DROP TABLE IF EXISTS `base_lang`;
CREATE TABLE `base_lang`  (
  `f_no` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `f_regtime` datetime(0) NULL DEFAULT NULL,
  `f_updatetime` datetime(0) NULL DEFAULT NULL,
  `f_isdelete` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `f_shortname` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `f_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `f_flag` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`f_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of base_lang
-- ----------------------------
INSERT INTO `base_lang` VALUES (1, NULL, NULL, '0', 'en', 'English', 'gb.svg');
INSERT INTO `base_lang` VALUES (2, NULL, NULL, '0', 'fr', 'Français', 'fr.svg');
INSERT INTO `base_lang` VALUES (3, NULL, NULL, '0', 'ru', 'русский язык', 'ru.svg');
INSERT INTO `base_lang` VALUES (4, NULL, NULL, '0', 'de', 'Deutsch', 'de.svg');
INSERT INTO `base_lang` VALUES (5, '2018-11-02 12:18:06', '2018-11-02 12:31:21', '0', 'cn', '汉语', 'cn.svg');
INSERT INTO `base_lang` VALUES (6, '2018-11-02 12:28:22', '2018-11-02 12:30:02', '0', 'jp', '日本語', 'jp.svg');

-- ----------------------------
-- Table structure for lang_words
-- ----------------------------
DROP TABLE IF EXISTS `lang_words`;
CREATE TABLE `lang_words`  (
  `f_no` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `f_regtime` datetime(0) NULL DEFAULT NULL,
  `f_updatetime` datetime(0) NULL DEFAULT NULL,
  `f_isdelete` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `f_key` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `f_lang` int(11) NULL DEFAULT NULL,
  `f_content` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`f_no`) USING BTREE,
  INDEX `f_key`(`f_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 364 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lang_words
-- ----------------------------
INSERT INTO `lang_words` VALUES (1, '2018-10-31 11:54:02', '2018-10-31 11:54:02', '0', 'text_user_first_page_face', 1, 'Connect with local cooks and enjoy delicious authentic food');
INSERT INTO `lang_words` VALUES (2, '2018-10-31 11:54:02', '2018-10-31 11:54:02', '0', 'text_user_first_page_description', 1, 'Find local cooks and chefs in your area and order food takeout, sit-in, or hfor free');
INSERT INTO `lang_words` VALUES (3, '2018-10-31 11:54:02', '2018-10-31 11:54:02', '0', 'text_user_select_location', 1, 'Select Location....');
INSERT INTO `lang_words` VALUES (4, '2018-10-31 11:54:02', '2018-10-31 11:54:02', '0', 'text_user_choose_location', 1, 'Choose Location');
INSERT INTO `lang_words` VALUES (5, NULL, NULL, '0', 'text_user_order_food', 1, 'Order Food\r\n');
INSERT INTO `lang_words` VALUES (6, NULL, NULL, '0', 'text_user_delivery_or_take_out', 1, 'Delivery or take out');
INSERT INTO `lang_words` VALUES (7, NULL, NULL, '0', 'text_user_search_cooker', 1, 'Search Cooker');
INSERT INTO `lang_words` VALUES (8, NULL, NULL, '0', 'text_user_matched_cooker_count_description', 1, 'cookers matched your location');
INSERT INTO `lang_words` VALUES (9, NULL, NULL, '0', 'text_user_popular_dishes_this_month', 1, 'Popular dishes this month');
INSERT INTO `lang_words` VALUES (10, NULL, NULL, '0', 'text_user_page_description_2', 1, 'The easiest way to get homemade food right to your door');
INSERT INTO `lang_words` VALUES (11, NULL, NULL, '0', 'text_user_review_count_text', 1, 'Reviews');
INSERT INTO `lang_words` VALUES (12, NULL, NULL, '0', 'text_user_btn_order_now', 1, 'Order Now');
INSERT INTO `lang_words` VALUES (13, NULL, NULL, '0', 'text_user_three_step', 1, 'Easy 3 Step Order');
INSERT INTO `lang_words` VALUES (14, NULL, NULL, '0', 'text_user_three_step_first_step', 1, 'Choose a local cook');
INSERT INTO `lang_words` VALUES (15, NULL, NULL, '0', 'text_user_three_step_first_step_description', 1, 'Connect and find local cooks on our app and scroll through their menus.');
INSERT INTO `lang_words` VALUES (16, NULL, NULL, '0', 'text_user_three_step_second_step', 1, 'Pick a tasty dish');
INSERT INTO `lang_words` VALUES (17, NULL, NULL, '0', 'text_user_three_step_second_step_description', 1, 'Select your meal and when you’d like it made.');
INSERT INTO `lang_words` VALUES (18, NULL, NULL, '0', 'text_user_three_step_third_step', 1, 'Delivery Method');
INSERT INTO `lang_words` VALUES (19, NULL, NULL, '0', 'text_user_three_step_third_step_description', 1, 'Choose to get your meal delivered, picked up or sit in and connect with your cook and others over delicious homemade cooking');
INSERT INTO `lang_words` VALUES (20, NULL, NULL, '0', 'text_category_meal_type', 1, 'Meal Type');
INSERT INTO `lang_words` VALUES (21, NULL, NULL, '0', 'text_category_ingredient', 1, 'Ingredient');
INSERT INTO `lang_words` VALUES (22, NULL, NULL, '0', 'text_category_diet_and_health', 1, 'Diet and Health');
INSERT INTO `lang_words` VALUES (23, NULL, NULL, '0', 'text_category_seasonal', 1, 'Seasonal');
INSERT INTO `lang_words` VALUES (24, NULL, NULL, '0', 'text_category_dish_type', 1, 'Dish Type');
INSERT INTO `lang_words` VALUES (25, NULL, NULL, '0', 'text_category_cooking_style', 1, 'Cooking Style');
INSERT INTO `lang_words` VALUES (26, NULL, NULL, '0', 'text_category_world_cuisine', 1, 'World Cuisine');
INSERT INTO `lang_words` VALUES (27, NULL, NULL, '0', 'text_category_breakfast_and_brunch', 1, 'Breakfast and Brunch');
INSERT INTO `lang_words` VALUES (28, NULL, NULL, '0', 'text_category_desserts', 1, 'Desserts');
INSERT INTO `lang_words` VALUES (29, NULL, NULL, '0', 'text_category_dinners', 1, 'Dinners');
INSERT INTO `lang_words` VALUES (30, NULL, NULL, '0', 'text_category_lunch', 1, 'Lunch');
INSERT INTO `lang_words` VALUES (31, NULL, NULL, '0', 'text_category_beef', 1, 'Beef');
INSERT INTO `lang_words` VALUES (32, NULL, NULL, '0', 'text_category_beans_and_legumes', 1, 'Beans and Legumes');
INSERT INTO `lang_words` VALUES (33, NULL, NULL, '0', 'text_category_chicken_recipes', 1, 'Chicken Recipes');
INSERT INTO `lang_words` VALUES (34, NULL, NULL, '0', 'text_category_chocolate', 1, 'Chocolate');
INSERT INTO `lang_words` VALUES (35, NULL, NULL, '0', 'text_category_fruit', 1, 'Fruit');
INSERT INTO `lang_words` VALUES (36, NULL, NULL, '0', 'text_category_game_meats', 1, 'Game Meats');
INSERT INTO `lang_words` VALUES (37, NULL, NULL, '0', 'text_category_grains', 1, 'Grains');
INSERT INTO `lang_words` VALUES (38, NULL, NULL, '0', 'text_category_mushrooms', 1, 'Mushrooms');
INSERT INTO `lang_words` VALUES (39, NULL, NULL, '0', 'text_category_pasta', 1, 'Pasta');
INSERT INTO `lang_words` VALUES (40, NULL, NULL, '0', 'text_category_pork_recipes', 1, 'Pork Recipes');
INSERT INTO `lang_words` VALUES (41, NULL, NULL, '0', 'text_category_potatoes', 1, 'Potatoes');
INSERT INTO `lang_words` VALUES (42, NULL, NULL, '0', 'text_category_poultry', 1, 'Poultry');
INSERT INTO `lang_words` VALUES (43, NULL, NULL, '0', 'text_category_rice', 1, 'Rice');
INSERT INTO `lang_words` VALUES (44, NULL, NULL, '0', 'text_category_salmon', 1, 'Salmon');
INSERT INTO `lang_words` VALUES (45, NULL, NULL, '0', 'text_category_seafood', 1, 'Seafood');
INSERT INTO `lang_words` VALUES (46, NULL, NULL, '0', 'text_category_shrimp', 1, 'Shrimp');
INSERT INTO `lang_words` VALUES (47, NULL, NULL, '0', 'text_category_tofu_and_tempeh', 1, 'Tofu and Tempeh');
INSERT INTO `lang_words` VALUES (48, NULL, NULL, '0', 'text_category_turkey', 1, 'Turkey');
INSERT INTO `lang_words` VALUES (49, NULL, NULL, '0', 'text_category_vegetable_recipes', 1, 'Vegetable Recipes');
INSERT INTO `lang_words` VALUES (50, NULL, NULL, '0', 'text_category_diabetic', 1, 'Diabetic');
INSERT INTO `lang_words` VALUES (51, NULL, NULL, '0', 'text_category_low_carb_recipes', 1, 'Low Carb Recipes');
INSERT INTO `lang_words` VALUES (52, NULL, NULL, '0', 'text_category_dairy_free_recipes', 1, 'Dairy Free Recipes');
INSERT INTO `lang_words` VALUES (53, NULL, NULL, '0', 'text_category_gluten_free', 1, 'Gluten Free');
INSERT INTO `lang_words` VALUES (54, NULL, NULL, '0', 'text_category_healthy', 1, 'Healthy');
INSERT INTO `lang_words` VALUES (55, NULL, NULL, '0', 'text_category_heart_healthy_recipes', 1, 'Heart-Healthy Recipes');
INSERT INTO `lang_words` VALUES (56, NULL, NULL, '0', 'text_category_high_fiber_recipes', 1, 'High Fiber Recipes');
INSERT INTO `lang_words` VALUES (57, NULL, NULL, '0', 'text_category_low_calorie', 1, 'Low Calorie');
INSERT INTO `lang_words` VALUES (58, NULL, NULL, '0', 'text_category_low_cholesterol_recipes', 1, 'Low Cholesterol Recipes');
INSERT INTO `lang_words` VALUES (59, NULL, NULL, '0', 'text_category_low_fat', 1, 'Low Fat');
INSERT INTO `lang_words` VALUES (60, NULL, NULL, '0', 'text_category_weight_loss_recipes', 1, 'Weight-Loss Recipes');
INSERT INTO `lang_words` VALUES (61, NULL, NULL, '0', 'text_category_4th_of_july', 1, '4th of July');
INSERT INTO `lang_words` VALUES (62, NULL, NULL, '0', 'text_category_baby_shower', 1, 'Baby Shower');
INSERT INTO `lang_words` VALUES (63, NULL, NULL, '0', 'text_category_birthday', 1, 'Birthday');
INSERT INTO `lang_words` VALUES (64, NULL, NULL, '0', 'text_category_christmas', 1, 'Christmas');
INSERT INTO `lang_words` VALUES (65, NULL, NULL, '0', 'text_category_christmas_cookies', 1, 'Christmas Cookies');
INSERT INTO `lang_words` VALUES (66, NULL, NULL, '0', 'text_category_cinco_de_mayo', 1, 'Cinco de Mayo');
INSERT INTO `lang_words` VALUES (67, NULL, NULL, '0', 'text_category_easter_recipes', 1, 'Easter Recipes');
INSERT INTO `lang_words` VALUES (68, NULL, NULL, '0', 'text_category_football', 1, 'Football');
INSERT INTO `lang_words` VALUES (69, NULL, NULL, '0', 'text_category_halloween', 1, 'Halloween');
INSERT INTO `lang_words` VALUES (70, NULL, NULL, '0', 'text_category_hanukkah', 1, 'Hanukkah');
INSERT INTO `lang_words` VALUES (71, NULL, NULL, '0', 'text_category_mother\'s_day', 1, 'Mother\'s Day');
INSERT INTO `lang_words` VALUES (72, NULL, NULL, '0', 'text_category_new_year', 1, 'aYear');
INSERT INTO `lang_words` VALUES (73, NULL, NULL, '0', 'text_category_passover', 1, 'Passover');
INSERT INTO `lang_words` VALUES (74, NULL, NULL, '0', 'text_category_ramadan', 1, 'Ramadan');
INSERT INTO `lang_words` VALUES (75, NULL, NULL, '0', 'text_category_st._patrick\'s_day', 1, 'St. Patrick\'s Day');
INSERT INTO `lang_words` VALUES (76, NULL, NULL, '0', 'text_category_thanksgiving', 1, 'Thanksgiving');
INSERT INTO `lang_words` VALUES (77, NULL, NULL, '0', 'text_category_valentines_day', 1, 'Valentines Day');
INSERT INTO `lang_words` VALUES (78, NULL, NULL, '0', 'text_category_more_holidays_and_events', 1, 'More Holidays and Events');
INSERT INTO `lang_words` VALUES (79, NULL, NULL, '0', 'text_category_appetizers_snacks', 1, 'Appetizers & Snacks');
INSERT INTO `lang_words` VALUES (80, NULL, NULL, '0', 'text_category_bread_recipes', 1, 'Bread Recipes');
INSERT INTO `lang_words` VALUES (81, NULL, NULL, '0', 'text_category_cake_recipes', 1, 'Cake Recipes');
INSERT INTO `lang_words` VALUES (82, NULL, NULL, '0', 'text_category_candy_and_fudge', 1, 'Candy and Fudge');
INSERT INTO `lang_words` VALUES (83, NULL, NULL, '0', 'text_category_casserole_recipes', 1, 'Casserole Recipes');
INSERT INTO `lang_words` VALUES (84, NULL, NULL, '0', 'text_category_christmas_cookies', 1, 'Christmas Cookies');
INSERT INTO `lang_words` VALUES (85, NULL, NULL, '0', 'text_category_cocktail_recipes', 1, 'Cocktail Recipes');
INSERT INTO `lang_words` VALUES (86, NULL, NULL, '0', 'text_category_cookie_recipes', 1, 'Cookie Recipes');
INSERT INTO `lang_words` VALUES (87, NULL, NULL, '0', 'text_category_mac_and_cheese_recipes', 1, 'Mac and Cheese Recipes');
INSERT INTO `lang_words` VALUES (88, NULL, NULL, '0', 'text_category_main_dishes', 1, 'Main Dishes');
INSERT INTO `lang_words` VALUES (89, NULL, NULL, '0', 'text_category_pasta_salad_recipes', 1, 'Pasta Salad Recipes');
INSERT INTO `lang_words` VALUES (90, NULL, NULL, '0', 'text_category_pasta_recipes', 1, 'Pasta Recipes');
INSERT INTO `lang_words` VALUES (91, NULL, NULL, '0', 'text_category_pie_recipes', 1, 'Pie Recipes');
INSERT INTO `lang_words` VALUES (92, NULL, NULL, '0', 'text_category_pizza', 1, 'Pizza');
INSERT INTO `lang_words` VALUES (93, NULL, NULL, '0', 'text_category_sandwiches', 1, 'Sandwiches');
INSERT INTO `lang_words` VALUES (94, NULL, NULL, '0', 'text_category_sauces_and_condiments', 1, 'Sauces and Condiments');
INSERT INTO `lang_words` VALUES (95, NULL, NULL, '0', 'text_category_smoothie_recipes', 1, 'Smoothie Recipes');
INSERT INTO `lang_words` VALUES (96, NULL, NULL, '0', 'text_category_soups,_stew,_and_chili_recipes', 1, 'Soups, Stew, and Chili Recipes');
INSERT INTO `lang_words` VALUES (97, NULL, NULL, '0', 'text_category_bbq_grilling', 1, 'BBQ & Grilling');
INSERT INTO `lang_words` VALUES (98, NULL, NULL, '0', 'text_category_budget_cooking', 1, 'Budget Cooking');
INSERT INTO `lang_words` VALUES (99, NULL, NULL, '0', 'text_category_clean_eating', 1, 'Clean-Eating');
INSERT INTO `lang_words` VALUES (100, NULL, NULL, '0', 'text_category_cooking_for_kids', 1, 'Cooking for Kids');
INSERT INTO `lang_words` VALUES (101, NULL, NULL, '0', 'text_category_cooking_for_two', 1, 'Cooking for Two');
INSERT INTO `lang_words` VALUES (102, NULL, NULL, '0', 'text_category_gourmet', 1, 'Gourmet');
INSERT INTO `lang_words` VALUES (103, NULL, NULL, '0', 'text_category_paleo', 1, 'Paleo');
INSERT INTO `lang_words` VALUES (104, NULL, NULL, '0', 'text_category_pressure_cooker', 1, 'Pressure Cooker');
INSERT INTO `lang_words` VALUES (105, NULL, NULL, '0', 'text_category_quick_easy', 1, 'Quick & Easy');
INSERT INTO `lang_words` VALUES (106, NULL, NULL, '0', 'text_category_slow_cooker', 1, 'Slow Cooker');
INSERT INTO `lang_words` VALUES (107, NULL, NULL, '0', 'text_category_vegan', 1, 'Vegan');
INSERT INTO `lang_words` VALUES (108, NULL, NULL, '0', 'text_category_vegetarian', 1, 'Vegetarian');
INSERT INTO `lang_words` VALUES (109, NULL, NULL, '0', 'text_category_chinese', 1, 'Chinese');
INSERT INTO `lang_words` VALUES (110, NULL, NULL, '0', 'text_category_indian', 1, 'Indian');
INSERT INTO `lang_words` VALUES (111, NULL, NULL, '0', 'text_category_italian', 1, 'Italian');
INSERT INTO `lang_words` VALUES (112, NULL, NULL, '0', 'text_category_mexican', 1, 'Mexican');
INSERT INTO `lang_words` VALUES (113, NULL, NULL, '0', 'text_category_southern', 1, 'Southern');
INSERT INTO `lang_words` VALUES (114, NULL, NULL, '0', 'text_category_thai', 1, 'Thai');
INSERT INTO `lang_words` VALUES (115, NULL, NULL, '0', 'text_category_all_world_cuisine', 1, 'All World Cuisine');
INSERT INTO `lang_words` VALUES (116, NULL, NULL, '0', 'text_category_wine', 1, 'Wine');
INSERT INTO `lang_words` VALUES (117, NULL, NULL, '0', 'text_user_three_step_description', 1, 'Multiple online payment methods available on our app. Keeor is simply an easy way to connect and enjoy homemade food from all over the world.');
INSERT INTO `lang_words` VALUES (118, NULL, NULL, '0', 'text_user_featured_cooker', 1, 'Featured Cookers');
INSERT INTO `lang_words` VALUES (119, NULL, NULL, '0', 'text_user_price_min_text', 1, 'Min');
INSERT INTO `lang_words` VALUES (120, NULL, NULL, '0', 'text_user_price_max_text', 1, 'Max');
INSERT INTO `lang_words` VALUES (121, NULL, NULL, '0', 'text_user_time_minute_text', 1, 'min');
INSERT INTO `lang_words` VALUES (122, NULL, NULL, '0', 'text_user_text_become_a_doctor', 1, 'Become a cooker');
INSERT INTO `lang_words` VALUES (123, NULL, NULL, '0', 'text_user_text_become_a_doctor_description', 1, 'Join the thousands of other cookers who benefit from having their menus on');
INSERT INTO `lang_words` VALUES (124, NULL, NULL, '0', 'text_user_keeor_directory', 1, 'Keeor directory');
INSERT INTO `lang_words` VALUES (125, NULL, NULL, '0', 'text_user_i_am_cooker', 1, 'I‘m cooker');
INSERT INTO `lang_words` VALUES (126, NULL, NULL, '0', 'text_user_title', 1, 'Homeat');
INSERT INTO `lang_words` VALUES (127, NULL, NULL, '0', 'text_user_menu_home', 1, 'Home');
INSERT INTO `lang_words` VALUES (128, NULL, NULL, '0', 'text_user_menu_orders', 1, 'Orders');
INSERT INTO `lang_words` VALUES (129, NULL, NULL, '0', 'text_user_menu_cart', 1, 'Cart');
INSERT INTO `lang_words` VALUES (130, NULL, NULL, '0', 'text_user_menu_profile', 1, 'Profile');
INSERT INTO `lang_words` VALUES (131, NULL, NULL, '0', 'text_user_menu_login', 1, 'Login');
INSERT INTO `lang_words` VALUES (132, NULL, NULL, '0', 'text_user_menu_register', 1, 'Register');
INSERT INTO `lang_words` VALUES (133, NULL, NULL, '0', 'text_user_menu_languages', 1, 'Languages');
INSERT INTO `lang_words` VALUES (134, NULL, NULL, '0', 'text_user_step_1', 1, 'Choose Your Location');
INSERT INTO `lang_words` VALUES (135, NULL, NULL, '0', 'text_user_step_2', 1, 'Choose Cooker');
INSERT INTO `lang_words` VALUES (136, NULL, NULL, '0', 'text_user_step_3', 1, 'Pick Your favorite food');
INSERT INTO `lang_words` VALUES (137, NULL, NULL, '0', 'text_user_step_4', 1, 'Order and Pay online');
INSERT INTO `lang_words` VALUES (138, NULL, NULL, '0', 'text_user_page_footer_1', 1, 'Homemade goodness at the click of a button');
INSERT INTO `lang_words` VALUES (139, NULL, NULL, '0', 'text_user_page_footer_2', 1, 'Connect with chefs, and cooks and enjoy delicious homemade food anywhere around the world. Download our app for free and start ordering today.');
INSERT INTO `lang_words` VALUES (140, NULL, NULL, '0', 'text_user_btn_download_app', 1, 'Download App');
INSERT INTO `lang_words` VALUES (141, NULL, NULL, '0', 'text_user_page_footer_payment_options', 1, 'Payment Options');
INSERT INTO `lang_words` VALUES (142, NULL, NULL, '0', 'text_user_text_address', 1, 'Address');
INSERT INTO `lang_words` VALUES (143, NULL, NULL, '0', 'text_user_page_footer_page_description', 1, 'Concept design of oline food order and deliveye,planned as cooker directory');
INSERT INTO `lang_words` VALUES (144, NULL, NULL, '0', 'text_user_text_phone', 1, 'Phone');
INSERT INTO `lang_words` VALUES (145, NULL, NULL, '0', 'text_user_text_addition_information', 1, 'Addition informations');
INSERT INTO `lang_words` VALUES (146, NULL, NULL, '0', 'text_user_text_addition_description', 1, 'Join the thousands of other restaurants who benefit from having their menus on TakeOff');
INSERT INTO `lang_words` VALUES (147, NULL, NULL, '0', 'text_user_searched_result_count', 1, 'Results so far');
INSERT INTO `lang_words` VALUES (148, NULL, NULL, '0', 'text_user_sort_type_name', 1, 'Sort By Name');
INSERT INTO `lang_words` VALUES (149, NULL, NULL, '0', 'text_user_sort_type_delivery_time', 1, 'Sort By Delivery Time');
INSERT INTO `lang_words` VALUES (150, NULL, NULL, '0', 'text_user_sort_type_min_price', 1, 'Sort By Min Price');
INSERT INTO `lang_words` VALUES (151, NULL, NULL, '0', 'text_user_placeholder_search_cooker', 1, 'Search your favorite cooker');
INSERT INTO `lang_words` VALUES (152, NULL, NULL, '0', 'text_user_text_choose_cuisine', 1, 'Choose Cuisine');
INSERT INTO `lang_words` VALUES (153, NULL, NULL, '0', 'text_user_btn_category_select', 1, 'Select');
INSERT INTO `lang_words` VALUES (154, NULL, NULL, '0', 'text_user_btn_category_deselect', 1, 'Reset All');
INSERT INTO `lang_words` VALUES (155, NULL, NULL, '0', 'text_user_text_price_range', 1, 'Price range');
INSERT INTO `lang_words` VALUES (156, NULL, NULL, '0', 'text_user_text_price_filter', 1, 'Filter by price');
INSERT INTO `lang_words` VALUES (157, NULL, NULL, '0', 'text_user_text_browse_categories', 1, 'Browse All Categories');
INSERT INTO `lang_words` VALUES (158, NULL, NULL, '0', 'text_user_btn_select', 1, 'Select');
INSERT INTO `lang_words` VALUES (159, NULL, NULL, '0', 'text_user_btn_cancel', 1, 'Cancel');
INSERT INTO `lang_words` VALUES (160, NULL, NULL, '0', 'text_user_btn_view_menu', 1, 'View Menu');
INSERT INTO `lang_words` VALUES (161, NULL, NULL, '0', 'text_user_text_restaurant_closed', 1, 'Closed');
INSERT INTO `lang_words` VALUES (162, NULL, NULL, '0', 'text_user_text_restaurant_open', 1, 'Open');
INSERT INTO `lang_words` VALUES (163, NULL, NULL, '0', 'text_user_text_choose_menu', 1, 'Choose Menu');
INSERT INTO `lang_words` VALUES (164, NULL, NULL, '0', 'text_user_text_all', 1, 'All');
INSERT INTO `lang_words` VALUES (165, NULL, NULL, '0', 'text_user_text_review', 1, 'Review');
INSERT INTO `lang_words` VALUES (166, NULL, NULL, '0', 'text_user_text_no_review', 1, 'There is no review yet.');
INSERT INTO `lang_words` VALUES (167, NULL, NULL, '0', 'text_user_text_review_item_1', 1, 'Taste');
INSERT INTO `lang_words` VALUES (168, NULL, NULL, '0', 'text_user_text_review_item_2', 1, 'Amount');
INSERT INTO `lang_words` VALUES (169, NULL, NULL, '0', 'text_user_text_review_item_3', 1, 'Delivery');
INSERT INTO `lang_words` VALUES (170, NULL, NULL, '0', 'text_user_text_your_shopping_cart', 1, 'Your Shopping Cart');
INSERT INTO `lang_words` VALUES (171, NULL, NULL, '0', 'text_user_text_price_total', 1, 'TOTAL');
INSERT INTO `lang_words` VALUES (172, NULL, NULL, '0', 'text_user_text_free_shipping', 1, 'Free Shipping');
INSERT INTO `lang_words` VALUES (173, NULL, NULL, '0', 'text_user_btn_checkout', 1, 'Checkout');
INSERT INTO `lang_words` VALUES (174, NULL, NULL, '0', 'text_user_text_hint_sign_register1', 1, 'Do you have an account? If you have, you should');
INSERT INTO `lang_words` VALUES (175, NULL, NULL, '0', 'text_user_text_sign_in', 1, 'sign in');
INSERT INTO `lang_words` VALUES (176, NULL, NULL, '0', 'text_user_text_hint_sign_register2', 1, 'or you should');
INSERT INTO `lang_words` VALUES (177, NULL, NULL, '0', 'text_user_text_register', 1, 'register');
INSERT INTO `lang_words` VALUES (178, NULL, NULL, '0', 'text_user_text_hint_sign_register3', 1, 'and sign in.');
INSERT INTO `lang_words` VALUES (179, NULL, NULL, '0', 'text_user_text_cart_summary', 1, 'Cart summary');
INSERT INTO `lang_words` VALUES (180, NULL, NULL, '0', 'text_user_text_first_name', 1, 'First Name');
INSERT INTO `lang_words` VALUES (181, NULL, NULL, '0', 'text_user_text_last_name', 1, 'Last Name');
INSERT INTO `lang_words` VALUES (182, NULL, NULL, '0', 'text_user_text_country', 1, 'Country');
INSERT INTO `lang_words` VALUES (183, NULL, NULL, '0', 'text_user_text_company_name', 1, 'Company Name');
INSERT INTO `lang_words` VALUES (184, NULL, NULL, '0', 'text_user_text_full_address', 1, 'Full Address');
INSERT INTO `lang_words` VALUES (185, NULL, NULL, '0', 'text_user_text_city_state', 1, 'City / State');
INSERT INTO `lang_words` VALUES (186, NULL, NULL, '0', 'text_user_text_zip_postal_code', 1, 'Zip/ Postal Code');
INSERT INTO `lang_words` VALUES (187, NULL, NULL, '0', 'text_user_text_email_address', 1, 'Email Address');
INSERT INTO `lang_words` VALUES (188, NULL, NULL, '0', 'text_user_text_phone', 1, 'Phone Number');
INSERT INTO `lang_words` VALUES (189, NULL, NULL, '0', 'text_user_text_note', 1, 'Note');
INSERT INTO `lang_words` VALUES (190, NULL, NULL, '0', 'text_user_placeholder_input_note', 1, 'Input a note.');
INSERT INTO `lang_words` VALUES (191, NULL, NULL, '0', 'text_user_cart_subtotal', 1, 'Cart Subtotal');
INSERT INTO `lang_words` VALUES (192, NULL, NULL, '0', 'text_user_cart_amount', 1, 'Cart amount');
INSERT INTO `lang_words` VALUES (193, NULL, NULL, '0', 'text_user_cart_shipping_and_handling', 1, 'Shipping & Handling');
INSERT INTO `lang_words` VALUES (194, NULL, NULL, '0', 'text_user_cart_total', 1, 'Total');
INSERT INTO `lang_words` VALUES (195, NULL, NULL, '0', 'text_user_text_delivery_method_delivery', 1, 'Delivery');
INSERT INTO `lang_words` VALUES (196, NULL, NULL, '0', 'text_user_text_delivery_method_takeout', 1, 'Takeout');
INSERT INTO `lang_words` VALUES (197, NULL, NULL, '0', 'text_user_btn_pay_now', 1, 'Pay now');
INSERT INTO `lang_words` VALUES (198, NULL, NULL, '0', 'text_user_alert_empty_cart', 1, 'Your cart is empty.');
INSERT INTO `lang_words` VALUES (199, NULL, NULL, '0', 'text_user_alert_firstname', 1, 'Confirm your first name, please.');
INSERT INTO `lang_words` VALUES (200, NULL, NULL, '0', 'text_user_alert_lastname', 1, 'Confirm your last name, please.');
INSERT INTO `lang_words` VALUES (201, NULL, NULL, '0', 'text_user_alert_country', 1, 'Confirm your country, please.');
INSERT INTO `lang_words` VALUES (202, NULL, NULL, '0', 'text_user_alert_company', 1, 'Confirm your company, please.');
INSERT INTO `lang_words` VALUES (203, NULL, NULL, '0', 'text_user_alert_address', 1, 'Confirm your address, please.');
INSERT INTO `lang_words` VALUES (204, NULL, NULL, '0', 'text_user_alert_city', 1, 'Confirm your city, please.');
INSERT INTO `lang_words` VALUES (205, NULL, NULL, '0', 'text_user_alert_zip', 1, 'Confirm your zip, please.');
INSERT INTO `lang_words` VALUES (206, NULL, NULL, '0', 'text_user_alert_email', 1, 'Confirm your email, please.');
INSERT INTO `lang_words` VALUES (207, NULL, NULL, '0', 'text_user_alert_phone', 1, 'Confirm your phone, please.');
INSERT INTO `lang_words` VALUES (208, NULL, NULL, '0', 'text_user_text_buy_wine', 1, 'Would you like buy some WINE?');
INSERT INTO `lang_words` VALUES (209, NULL, NULL, '0', 'text_user_text_wine_menu', 1, 'Wine Menu');
INSERT INTO `lang_words` VALUES (210, NULL, NULL, '0', 'text_user_text_price', 1, 'Price');
INSERT INTO `lang_words` VALUES (211, NULL, NULL, '0', 'text_user_text_amount', 1, 'Amount');
INSERT INTO `lang_words` VALUES (212, NULL, NULL, '0', 'text_user_text_show_order_no', 1, 'Your order has been accepted. Your order no is');
INSERT INTO `lang_words` VALUES (213, NULL, NULL, '0', 'text_user_text_visit_order', 1, 'To check, visit');
INSERT INTO `lang_words` VALUES (214, NULL, NULL, '0', 'text_user_text_invalid_location', 1, 'The location is invalid.');
INSERT INTO `lang_words` VALUES (215, NULL, NULL, '0', 'text_user_text_password', 1, 'Password');
INSERT INTO `lang_words` VALUES (216, NULL, NULL, '0', 'text_user_text_rpassword', 1, 'Repeat password');
INSERT INTO `lang_words` VALUES (217, NULL, NULL, '0', 'text_user_text_captcha_image', 1, 'Captcha Image');
INSERT INTO `lang_words` VALUES (218, NULL, NULL, '0', 'text_user_text_captcha_code', 1, 'Captcha Code');
INSERT INTO `lang_words` VALUES (219, NULL, NULL, '0', 'text_user_placeholder_firstname', 1, 'Enter first name');
INSERT INTO `lang_words` VALUES (220, NULL, NULL, '0', 'text_user_placeholder_lastname', 1, 'Enter last name');
INSERT INTO `lang_words` VALUES (221, NULL, NULL, '0', 'text_user_placeholder_email', 1, 'Enter email');
INSERT INTO `lang_words` VALUES (222, NULL, NULL, '0', 'text_user_placeholder_phone', 1, 'Enter phone number');
INSERT INTO `lang_words` VALUES (223, NULL, NULL, '0', 'text_user_placeholder_password', 1, 'Password');
INSERT INTO `lang_words` VALUES (224, NULL, NULL, '0', 'text_user_placeholder_rpassword', 1, 'Password');
INSERT INTO `lang_words` VALUES (225, NULL, NULL, '0', 'text_user_placeholder_captcha_code', 1, 'Enter captcha code');
INSERT INTO `lang_words` VALUES (226, NULL, NULL, '0', 'text_user_btn_update', 1, 'Update');
INSERT INTO `lang_words` VALUES (227, NULL, NULL, '0', 'text_user_alert_password', 1, 'Confirm your password, please.');
INSERT INTO `lang_words` VALUES (228, NULL, NULL, '0', 'text_user_alert_password_min_six_letter', 1, 'Password should be at least 6 characters long.');
INSERT INTO `lang_words` VALUES (229, NULL, NULL, '0', 'text_user_alert_captcha_code', 1, 'Confirm you captcha code, please.');
INSERT INTO `lang_words` VALUES (230, NULL, NULL, '0', 'text_user_alert_get_captcha_code_error', 1, 'Get captcha code is failed.');
INSERT INTO `lang_words` VALUES (231, NULL, NULL, '0', 'text_user_alert_registeration_fail', 1, 'User registration has been failed.');
INSERT INTO `lang_words` VALUES (232, NULL, NULL, '0', 'text_user_alert_registeration_success', 1, 'Registration successfully. Please check your email inbox for verification.');
INSERT INTO `lang_words` VALUES (233, NULL, NULL, '0', 'text_user_alert_profile_update_success', 1, 'Successfully updated.');
INSERT INTO `lang_words` VALUES (234, NULL, NULL, '0', 'text_user_alert_profile_update_fail', 1, 'User updating failed.');
INSERT INTO `lang_words` VALUES (235, NULL, NULL, '0', 'text_user_alert_has_food_from_other_cooker', 1, 'There is already food from other cooker?');
INSERT INTO `lang_words` VALUES (236, NULL, NULL, '0', 'text_user_alert_confirm_empty_cart', 1, 'You can only pick from one cooker. Do wanna empty your cart?');
INSERT INTO `lang_words` VALUES (237, NULL, NULL, '0', 'text_user_btn_empty_cart', 1, 'Yes, empty it!');
INSERT INTO `lang_words` VALUES (238, NULL, NULL, '0', 'text_user_alert_emptied_cart_title', 1, 'Emptied!');
INSERT INTO `lang_words` VALUES (239, NULL, NULL, '0', 'text_user_alert_emptied_cart_detail', 1, 'Your cart has been emptied.');
INSERT INTO `lang_words` VALUES (240, NULL, NULL, '0', 'text_user_alert_buy_more_title', 1, 'Please buy more.');
INSERT INTO `lang_words` VALUES (241, NULL, NULL, '0', 'text_user_alert_min_price_is', 1, 'Min Price is');
INSERT INTO `lang_words` VALUES (242, NULL, NULL, '0', 'text_user_btn_register', 1, 'Register');
INSERT INTO `lang_words` VALUES (243, NULL, NULL, '0', 'text_user_btn_login', 1, 'Login');
INSERT INTO `lang_words` VALUES (244, NULL, NULL, '0', 'text_user_dont_share_email_explain', 1, 'We\"ll never share your email with anyone else.');
INSERT INTO `lang_words` VALUES (245, NULL, NULL, '0', 'text_cooker_text_dashboard', 1, 'Dashboard');
INSERT INTO `lang_words` VALUES (246, NULL, NULL, '0', 'text_cooker_title', 1, 'Homeat | Cooker');
INSERT INTO `lang_words` VALUES (247, NULL, NULL, '0', 'text_cooker_site_name', 1, 'HomeAt');
INSERT INTO `lang_words` VALUES (248, NULL, NULL, '0', 'text_cooker_text_greeting', 1, 'Hello,');
INSERT INTO `lang_words` VALUES (249, NULL, NULL, '0', 'text_cooker_text_my_profile', 1, 'My Profile');
INSERT INTO `lang_words` VALUES (250, NULL, NULL, '0', 'text_cooker_text_logout', 1, 'Logout');
INSERT INTO `lang_words` VALUES (251, NULL, NULL, '0', 'text_cooker_text_language', 1, 'Language');
INSERT INTO `lang_words` VALUES (252, NULL, NULL, '0', 'text_cooker_menu_dashboard', 1, 'Dashboard');
INSERT INTO `lang_words` VALUES (253, NULL, NULL, '0', 'text_cooker_menu_order', 1, 'Order');
INSERT INTO `lang_words` VALUES (254, NULL, NULL, '0', 'text_cooker_menu_food', 1, 'Food');
INSERT INTO `lang_words` VALUES (255, NULL, NULL, '0', 'text_cooker_menu_wine', 1, 'Wine');
INSERT INTO `lang_words` VALUES (256, NULL, NULL, '0', 'text_cooker_menu_profile', 1, 'Profile');
INSERT INTO `lang_words` VALUES (257, NULL, NULL, '0', 'text_cooker_text_dashboard_food_count', 1, 'Food Count');
INSERT INTO `lang_words` VALUES (258, NULL, NULL, '0', 'text_cooker_text_dashboard_food_count_detail', 1, 'Total Registered Foods');
INSERT INTO `lang_words` VALUES (259, NULL, NULL, '0', 'text_cooker_text_dashboard_orders', 1, 'Orders');
INSERT INTO `lang_words` VALUES (260, NULL, NULL, '0', 'text_cooker_text_dashboard_orders_detail', 1, 'Total Orders');
INSERT INTO `lang_words` VALUES (261, NULL, NULL, '0', 'text_cooker_text_dashboard_sales', 1, 'Sales');
INSERT INTO `lang_words` VALUES (262, NULL, NULL, '0', 'text_cooker_text_dashboard_sales_detail', 1, 'Total price');
INSERT INTO `lang_words` VALUES (263, NULL, NULL, '0', 'text_cooker_text_dashboard_popular', 1, 'Popular dishes');
INSERT INTO `lang_words` VALUES (264, NULL, NULL, '0', 'text_cooker_text_sales_count', 1, 'sales');
INSERT INTO `lang_words` VALUES (265, NULL, NULL, '0', 'text_cooker_pagename_order', 1, 'Orders');
INSERT INTO `lang_words` VALUES (266, NULL, NULL, '0', 'text_cooker_filtername_status', 1, 'Status');
INSERT INTO `lang_words` VALUES (267, NULL, NULL, '0', 'text_cooker_orderstatus_all', 1, 'All');
INSERT INTO `lang_words` VALUES (268, NULL, NULL, '0', 'text_cooker_orderstatus_waiting', 1, 'Waiting');
INSERT INTO `lang_words` VALUES (269, NULL, NULL, '0', 'text_cooker_orderstatus_accept', 1, 'Accepted');
INSERT INTO `lang_words` VALUES (270, NULL, NULL, '0', 'text_cooker_orderstatus_cancel', 1, 'Canceled');
INSERT INTO `lang_words` VALUES (271, NULL, NULL, '0', 'text_cooker_orderstatus_delivery', 1, 'Delivery');
INSERT INTO `lang_words` VALUES (272, NULL, NULL, '0', 'text_cooker_orderstatus_transaction', 1, 'Transaction');
INSERT INTO `lang_words` VALUES (273, NULL, NULL, '0', 'text_cooker_placeholder_search', 1, 'Search...');
INSERT INTO `lang_words` VALUES (274, NULL, NULL, '0', 'text_cooker_order_table_field_time', 1, 'Time');
INSERT INTO `lang_words` VALUES (275, NULL, NULL, '0', 'text_cooker_order_table_field_name', 1, 'Full Name');
INSERT INTO `lang_words` VALUES (276, NULL, NULL, '0', 'text_cooker_order_table_field_address', 1, 'Address');
INSERT INTO `lang_words` VALUES (277, NULL, NULL, '0', 'text_cooker_order_table_field_city', 1, 'City');
INSERT INTO `lang_words` VALUES (278, NULL, NULL, '0', 'text_cooker_order_table_field_email', 1, 'Email');
INSERT INTO `lang_words` VALUES (279, NULL, NULL, '0', 'text_cooker_order_table_field_phone', 1, 'Phone');
INSERT INTO `lang_words` VALUES (280, NULL, NULL, '0', 'text_cooker_order_table_field_payment', 1, 'Payment');
INSERT INTO `lang_words` VALUES (281, NULL, NULL, '0', 'text_cooker_order_table_field_total', 1, 'Total');
INSERT INTO `lang_words` VALUES (282, NULL, NULL, '0', 'text_cooker_order_table_field_amount', 1, 'Amount');
INSERT INTO `lang_words` VALUES (283, NULL, NULL, '0', 'text_cooker_order_table_field_note', 1, 'Note');
INSERT INTO `lang_words` VALUES (284, NULL, NULL, '0', 'text_cooker_order_table_field_status', 1, 'Status');
INSERT INTO `lang_words` VALUES (285, NULL, NULL, '0', 'text_cooker_order_table_field_action', 1, 'Action');
INSERT INTO `lang_words` VALUES (286, NULL, NULL, '0', 'text_cooker_order_action_accept', 1, 'Accept');
INSERT INTO `lang_words` VALUES (287, NULL, NULL, '0', 'text_cooker_order_action_cancel', 1, 'Cancel');
INSERT INTO `lang_words` VALUES (288, NULL, NULL, '0', 'text_cooker_order_action_delivery', 1, 'Delivery');
INSERT INTO `lang_words` VALUES (289, NULL, NULL, '0', 'text_cooker_order_action_transaction', 1, 'Transaction');
INSERT INTO `lang_words` VALUES (290, NULL, NULL, '0', 'text_cooker_order_detail_table_field_food', 1, 'Food');
INSERT INTO `lang_words` VALUES (291, NULL, NULL, '0', 'text_cooker_order_detail_table_field_type', 1, 'Type');
INSERT INTO `lang_words` VALUES (292, NULL, NULL, '0', 'text_cooker_order_detail_table_field_amount', 1, 'Amount');
INSERT INTO `lang_words` VALUES (293, NULL, NULL, '0', 'text_cooker_order_detail_table_field_price', 1, 'Price');
INSERT INTO `lang_words` VALUES (294, NULL, NULL, '0', 'text_cooker_order_detail_table_field_total', 1, 'Total');
INSERT INTO `lang_words` VALUES (295, NULL, NULL, '0', 'text_cooker_message_update_food_not_yours', 1, 'This is not your food.');
INSERT INTO `lang_words` VALUES (296, NULL, NULL, '0', 'text_cooker_message_update_food_invalid_id', 1, 'invalid food_id');
INSERT INTO `lang_words` VALUES (297, NULL, NULL, '0', 'text_cooker_message_update_food_no_data', 1, 'no data with no id');
INSERT INTO `lang_words` VALUES (298, NULL, NULL, '0', 'text_cooker_pagename_food_manage', 1, 'Food Manage');
INSERT INTO `lang_words` VALUES (299, NULL, NULL, '0', 'text_cooker_text_menus', 1, 'Menus');
INSERT INTO `lang_words` VALUES (300, NULL, NULL, '0', 'text_cooker_text_all', 1, 'All');
INSERT INTO `lang_words` VALUES (301, NULL, NULL, '0', 'text_cooker_text_foods', 1, 'Foods');
INSERT INTO `lang_words` VALUES (302, NULL, NULL, '0', 'text_cooker_foodstatus_active', 1, 'Active');
INSERT INTO `lang_words` VALUES (303, NULL, NULL, '0', 'text_cooker_foodstatus_deactive', 1, 'Deactive');
INSERT INTO `lang_words` VALUES (304, NULL, NULL, '0', 'text_cooker_foodstatus_all', 1, 'All');
INSERT INTO `lang_words` VALUES (305, NULL, NULL, '0', 'text_cooker_question_sure', 1, 'Are you sure?');
INSERT INTO `lang_words` VALUES (306, NULL, NULL, '0', 'text_cooker_warning_delete', 1, 'You won\'t be able to revert this');
INSERT INTO `lang_words` VALUES (307, NULL, NULL, '0', 'text_cooker_confirm_delete', 1, 'Yes, delete it!');
INSERT INTO `lang_words` VALUES (308, NULL, NULL, '0', 'text_cooker_notice_deleted', 1, 'Your food has been deleted');
INSERT INTO `lang_words` VALUES (309, NULL, NULL, '0', 'text_cooker_deleted', 1, 'Deleted');
INSERT INTO `lang_words` VALUES (310, NULL, NULL, '0', 'text_cooker_menu_deleted', 1, 'You menu has been deleted.');
INSERT INTO `lang_words` VALUES (311, NULL, NULL, '0', 'text_cooker_food_saved', 1, 'Your food has been saved');
INSERT INTO `lang_words` VALUES (312, NULL, NULL, '0', 'text_cooker_wine_saved', 1, 'Your wine has been saved');
INSERT INTO `lang_words` VALUES (313, NULL, NULL, '0', 'text_cooker_add_menu', 1, 'Add Menu');
INSERT INTO `lang_words` VALUES (314, NULL, NULL, '0', 'text_cooker_food_menus', 1, 'Food Menus');
INSERT INTO `lang_words` VALUES (315, NULL, NULL, '0', 'text_cooker_new_food', 1, 'New Food');
INSERT INTO `lang_words` VALUES (316, NULL, NULL, '0', 'text_cooker_id', 1, 'ID');
INSERT INTO `lang_words` VALUES (317, NULL, NULL, '0', 'text_cooker_name', 1, 'Name');
INSERT INTO `lang_words` VALUES (318, NULL, NULL, '0', 'text_cooker_price', 1, 'Price');
INSERT INTO `lang_words` VALUES (319, NULL, NULL, '0', 'text_cooker_wines', 1, 'Wines');
INSERT INTO `lang_words` VALUES (320, NULL, NULL, '0', 'text_cooker_wine_title_here', 1, 'Wine Title here.');
INSERT INTO `lang_words` VALUES (321, NULL, NULL, '0', 'text_cooker_description', 1, 'Description');
INSERT INTO `lang_words` VALUES (322, NULL, NULL, '0', 'text_cooker_wine_description', 1, 'Wine description');
INSERT INTO `lang_words` VALUES (323, NULL, NULL, '0', 'text_cooker_wine_deleted', 1, 'Your wine has been deleted');
INSERT INTO `lang_words` VALUES (324, NULL, NULL, '0', 'text_cooker_invalid_inputs', 1, 'You have some invalid inputs.');
INSERT INTO `lang_words` VALUES (325, NULL, NULL, '0', 'text_cooker_check_and_submit', 1, 'Check fields and submit again.');
INSERT INTO `lang_words` VALUES (326, NULL, NULL, '0', 'text_cooker_my_profile', 1, 'My Profile');
INSERT INTO `lang_words` VALUES (327, NULL, NULL, '0', 'text_cooker_your_account', 1, 'Your Account');
INSERT INTO `lang_words` VALUES (328, NULL, NULL, '0', 'text_cooker_current_password', 1, 'Current Password');
INSERT INTO `lang_words` VALUES (329, NULL, NULL, '0', 'text_cooker_new_passowrd', 1, 'New Password');
INSERT INTO `lang_words` VALUES (330, NULL, NULL, '0', 'text_cooker_confirm_password', 1, 'Confirm Password');
INSERT INTO `lang_words` VALUES (331, NULL, NULL, '0', 'text_cooker_phone_no', 1, 'Phone No.');
INSERT INTO `lang_words` VALUES (332, NULL, NULL, '0', 'text_cooker_min_price', 1, 'Min Price');
INSERT INTO `lang_words` VALUES (333, NULL, NULL, '0', 'text_cooker_location', 1, 'Location');
INSERT INTO `lang_words` VALUES (334, NULL, NULL, '0', 'text_cooker_opening_hours', 1, 'Opening Hours');
INSERT INTO `lang_words` VALUES (335, NULL, NULL, '0', 'text_cooker_delivery_hours', 1, 'Delivery Hours');
INSERT INTO `lang_words` VALUES (336, NULL, NULL, '0', 'text_cooker_delivery_limit', 1, 'Delivery Limit');
INSERT INTO `lang_words` VALUES (337, NULL, NULL, '0', 'text_cooker_save_changes', 1, 'Save changes');
INSERT INTO `lang_words` VALUES (338, NULL, NULL, '0', 'text_cooker_profile_updated', 1, 'Profile Updated');
INSERT INTO `lang_words` VALUES (339, NULL, NULL, '0', 'text_cooker_profile_update_failed', 1, 'Profile update has been failed');
INSERT INTO `lang_words` VALUES (340, NULL, NULL, '0', 'text_cooker_avatar_upload_fail', 1, 'Avatar image upload failed');
INSERT INTO `lang_words` VALUES (341, NULL, NULL, '0', 'text_cooker_current_password_incorrect', 1, 'Current password incorrect');
INSERT INTO `lang_words` VALUES (342, NULL, NULL, '0', 'text_monday', 1, 'Monday');
INSERT INTO `lang_words` VALUES (343, NULL, NULL, '0', 'text_tuesday', 1, 'Tuesday');
INSERT INTO `lang_words` VALUES (344, NULL, NULL, '0', 'text_wednesday', 1, 'Wednesday');
INSERT INTO `lang_words` VALUES (345, NULL, NULL, '0', 'text_thursday', 1, 'Thursday');
INSERT INTO `lang_words` VALUES (346, NULL, NULL, '0', 'text_friday', 1, 'Friday');
INSERT INTO `lang_words` VALUES (347, NULL, NULL, '0', 'text_saturday', 1, 'Saturday');
INSERT INTO `lang_words` VALUES (348, NULL, NULL, '0', 'text_sunday', 1, 'Sunday');
INSERT INTO `lang_words` VALUES (349, NULL, NULL, '0', 'text_cooker_food_name', 1, 'Food Name');
INSERT INTO `lang_words` VALUES (350, NULL, NULL, '0', 'text_cooker_enter_food_name', 1, 'Please enter your food name');
INSERT INTO `lang_words` VALUES (351, NULL, NULL, '0', 'text_cooker_enter_full_name', 1, 'Enter full name');
INSERT INTO `lang_words` VALUES (352, NULL, NULL, '0', 'text_cooker_menu', 1, 'Menu');
INSERT INTO `lang_words` VALUES (353, NULL, NULL, '0', 'text_question_choose_menu', 1, 'Which menu this food would be shown?');
INSERT INTO `lang_words` VALUES (354, NULL, NULL, '0', 'text_cooker_category', 1, 'Category');
INSERT INTO `lang_words` VALUES (355, NULL, NULL, '0', 'text_cooker_enter_your_food_category', 1, 'Please enter your food category');
INSERT INTO `lang_words` VALUES (356, NULL, NULL, '0', 'text_cooker_select_payment', 1, 'Select your payment method');
INSERT INTO `lang_words` VALUES (357, NULL, NULL, '0', 'text_cooker_keyword', 1, 'Keyword');
INSERT INTO `lang_words` VALUES (358, NULL, NULL, '0', 'text_btn_save', 1, 'Save');
INSERT INTO `lang_words` VALUES (359, NULL, NULL, '0', 'text_btn_back', 1, 'Back');
INSERT INTO `lang_words` VALUES (360, NULL, NULL, '0', 'text_notice_default_wil_be_shown', 1, 'Default values will be shown if you don\'t input');
INSERT INTO `lang_words` VALUES (361, NULL, NULL, '0', 'text_cooker_select_category', 1, 'Select Categories');
INSERT INTO `lang_words` VALUES (362, NULL, NULL, '0', 'text_cooker_input_keywords', 1, 'Input Keywords');
INSERT INTO `lang_words` VALUES (363, '2018-11-02 15:00:27', '2018-11-02 15:00:27', '0', 'text_category_desserts', 2, 'Test for Language manage');

-- ----------------------------
-- Table structure for tb_articles
-- ----------------------------
DROP TABLE IF EXISTS `tb_articles`;
CREATE TABLE `tb_articles`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `is_delete` int(1) NULL DEFAULT 0,
  `is_show` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_articles
-- ----------------------------
INSERT INTO `tb_articles` VALUES (1, 'Teste', 'avasdfasfwefwe', '2019-10-11 14:54:40', 0, 1);
INSERT INTO `tb_articles` VALUES (2, 'asd', 'fasdf', '2019-10-11 15:38:14', 0, 0);
INSERT INTO `tb_articles` VALUES (3, 'qwef', 'asvasdf', '2019-10-11 15:38:24', 0, 1);

-- ----------------------------
-- Table structure for tb_payments
-- ----------------------------
DROP TABLE IF EXISTS `tb_payments`;
CREATE TABLE `tb_payments`  (
  `txn_id` int(11) NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `PaymentMethod` varchar(0) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PayerStatus` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PayerMail` int(100) NULL DEFAULT NULL,
  `Total` decimal(19, 2) NULL DEFAULT NULL,
  `SubTotal` decimal(19, 2) NULL DEFAULT NULL,
  `Tax` decimal(19, 2) NULL DEFAULT NULL,
  `Payment_state` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CreateTime` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `UpdateTime` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tb_product
-- ----------------------------
DROP TABLE IF EXISTS `tb_product`;
CREATE TABLE `tb_product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(10, 0) NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(2) NULL DEFAULT 1,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `is_delete` int(1) NULL DEFAULT 0,
  `is_show` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 126 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_product
-- ----------------------------
INSERT INTO `tb_product` VALUES (123, 'SBA 8A DIY SMART KIT', '', 199, 'Instant Download Everything you need (forms, samples, narratives, tips, etc.) to prepare the 8(a) application yourself.  Over 140 pages.', '1049479600_new app.pdf', 1, NULL, 0, 1);
INSERT INTO `tb_product` VALUES (124, 'SELF WOSB CERTIFICATION SMART KIT', '', 199, 'Instant Download Everything you need (forms, samples, narratives, tips, etc.) to prepare the 8(a) application yourself.  Over 140 pages.', 'GrowthGet_Flyer.pdf', 1, NULL, 0, 1);
INSERT INTO `tb_product` VALUES (125, 'SELF WOSB CERTIFICATION SMART KIT ', '', 199, 'Instant Download Everything you need (forms, samples, narratives, tips, etc.) to prepare the 8(a) application yourself.  Over 140 pages.', 'Intellectual_Property_Transfer_Agreement.pdf', 1, NULL, 0, 1);

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lastname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `verify_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `security_id` int(11) NULL DEFAULT NULL,
  `security_answer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `info` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fee_type` int(11) NULL DEFAULT NULL,
  `delivery_hour` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `delivery_limit` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_type` int(11) NULL DEFAULT NULL,
  `min_price` decimal(10, 0) NULL DEFAULT NULL,
  `active` int(11) NOT NULL,
  `ingredient` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` datetime(0) NOT NULL,
  `payment_method` tinyint(3) NULL DEFAULT NULL,
  `open_hour` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `open_at` time(0) NULL DEFAULT NULL,
  `close_at` time(0) NULL DEFAULT NULL,
  `is_delete` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `Unique`(`email`, `phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'Jackson', 'Willian', 'admin@admin.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1-(234)-567-890', 'admin.jpg', NULL, 1, 'Donald', NULL, 'a', NULL, NULL, NULL, 1, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, NULL, '01:00:00', '23:50:00', 0);

SET FOREIGN_KEY_CHECKS = 1;
