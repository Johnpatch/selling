/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100116
 Source Host           : localhost:3306
 Source Schema         : selling

 Target Server Type    : MySQL
 Target Server Version : 100116
 File Encoding         : 65001

 Date: 12/10/2019 03:46:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_articles
-- ----------------------------
DROP TABLE IF EXISTS `tb_articles`;
CREATE TABLE `tb_articles`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `is_delete` int(1) NULL DEFAULT 0,
  `is_show` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_articles
-- ----------------------------
INSERT INTO `tb_articles` VALUES (1, 'Teste', 'avasdfasfwefwe', '2019-10-11 14:54:40', 0, 1);
INSERT INTO `tb_articles` VALUES (2, 'asd', 'fasdf', '2019-10-11 15:38:14', 0, 0);
INSERT INTO `tb_articles` VALUES (3, 'qwef', 'asvasdf', '2019-10-11 15:38:24', 0, 1);

-- ----------------------------
-- Table structure for tb_product
-- ----------------------------
DROP TABLE IF EXISTS `tb_product`;
CREATE TABLE `tb_product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(10, 0) NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(2) NULL DEFAULT 1,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `is_delete` int(1) NULL DEFAULT 0,
  `is_show` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 126 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_product
-- ----------------------------
INSERT INTO `tb_product` VALUES (123, 'SBA 8A DIY SMART KIT', '', 199, 'Instant Download Everything you need (forms, samples, narratives, tips, etc.) to prepare the 8(a) application yourself.  Over 140 pages.', '1049479600_new app.pdf', 1, NULL, 0, 1);
INSERT INTO `tb_product` VALUES (124, 'SELF WOSB CERTIFICATION SMART KIT', '', 199, 'Instant Download Everything you need (forms, samples, narratives, tips, etc.) to prepare the 8(a) application yourself.  Over 140 pages.', '', 1, NULL, 0, 1);
INSERT INTO `tb_product` VALUES (125, 'SELF WOSB CERTIFICATION SMART KIT ', '', 199, 'Instant Download Everything you need (forms, samples, narratives, tips, etc.) to prepare the 8(a) application yourself.  Over 140 pages.', '', 1, NULL, 0, 1);

SET FOREIGN_KEY_CHECKS = 1;
