<!DOCTYPE html>
<html lang="en" >
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        <title>Admin Management</title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        
        <!--end::Web font -->
        <!--begin::Base Styles -->
        <link href="<?php echo base_url();?>assets_m/metronic/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets_m/metronic/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
        <!--end::Base Styles -->
    </head>
    <!-- end::Head -->
    <!-- end::Body -->
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url(<?php echo base_url();?>assets_m/metronic/assets/app/media/img//bg/bg-1.jpg);">
                <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
                    <div class="m-login__container">
                        <div class="m-login__signin">
                            <form class="m-login__form m-form form-horizontal" name="login_form" id="login_form" method="POST" action="<?php echo site_url('admin/login');?>" role="form">
                            <?php if($this->session->flashdata('error_msg')){
                                echo '<div class="alert alert-danger"><strong>'.$this->session->flashdata('error_msg').'</strong></div>'; 
                            } ?>
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input"  id="email" type="text" placeholder="Email" name="email" autocomplete="off">
                                    <?php echo form_error('email'); ?>
                                </div>
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input m-login__form-input--last" id="password" type="password" placeholder="password" name="password">
                                    <?php echo form_error('password'); ?>
                                </div>
                                <div class="m-login__form-action">
                                    <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">
                                        Login
                                    </button>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Page -->
        <!--begin::Base Scripts -->
        <script src="<?php echo base_url();?>assets_m/metronic/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets_m/metronic/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Base Scripts -->   
        <!--begin::Page Snippets -->
        <script src="<?php echo base_url();?>assets_m/metronic/assets/snippets/pages/user/login.js" type="text/javascript"></script>
        <!--end::Page Snippets -->
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url(); ?>assets_m/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/detect.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/fastclick.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/waves.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/wow.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/jquery.scrollTo.min.js"></script>


        <script src="<?php echo base_url(); ?>assets_m/js/jquery.core.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/jquery.app.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets_m/plugins/parsleyjs/dist/parsley.min.js"></script>        
        
        <!-- jQuery  -->
        <script src="<?php echo base_url(); ?>assets_m/plugins/waypoints/lib/jquery.waypoints.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/plugins/counterup/jquery.counterup.min.js"></script>

        <!-- For getting local timezone -->
        <script src="<?php echo base_url();?>assets_m/js/jstz.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('form').parsley();

            });
        </script>
    </body>
    <!-- end::Body -->
</html>
