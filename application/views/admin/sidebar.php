        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
                <i class="la la-close"></i>
            </button>
            <div id="m_aside_left" class="side-menu m-grid__item  m-aside-left  m-aside-left--skin-dark ">
                <!-- BEGIN: Aside Menu -->
                <div 
                    id="m_ver_menu" 
                    class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
                    data-menu-vertical="true"
                     data-menu-scrollable="false" data-menu-dropdown-timeout="500"  
                    >
                    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow " style="padding-top: 0px;">
                    	<li class="m-menu__item <?php echo $this->common->get_menu('main');?>" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                            <a  href="<?php echo site_url('admin/main');?>" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-tabs"></i>
                                <span class="m-menu__link-text">
                                    Main
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php echo $this->common->get_menu('benefits');?>" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                            <a  href="<?php echo site_url('admin/benefits');?>" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-tabs"></i>
                                <span class="m-menu__link-text">
                                    Benefits
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php echo $this->common->get_menu('testimonials');?>" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                            <a  href="<?php echo site_url('admin/testimonials');?>" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-tabs"></i>
                                <span class="m-menu__link-text">
                                    Testimonials
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php echo $this->common->get_menu('about');?>" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                            <a  href="<?php echo site_url('admin/about');?>" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-tabs"></i>
                                <span class="m-menu__link-text">
                                    About Us
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php echo $this->common->get_menu('product');?>" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                            <a  href="<?php echo site_url('admin/product');?>" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-tabs"></i>
                                <span class="m-menu__link-text">
                                    Product
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php echo $this->common->get_menu('article');?>" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                            <a  href="<?php echo site_url('admin/article');?>" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-tabs"></i>
                                <span class="m-menu__link-text">
                                    Article
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- END: Aside Menu -->
            </div>
            <!-- END: Left Aside -->