<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url(); ?>assets_m/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_m/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_m/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_m/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_m/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_m/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets_m/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_m/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_m/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets_m/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets_m/pages/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<!-- END THEME LAYOUT SCRIPTS -->
</body>

<script>
    $(document).ready(function() {
        $('#btn_login').click(function() {
            $.ajax({
                'url' : '<?php echo site_url("admin/login/signin"); ?>',
                'type' : 'POST',
                'data' : $("#frm_login").serializeArray(),
                'dataType' : 'json',
                'success' : function(response) {
                    if (response.message !== '') {
                        $('.alert-danger > span').html(response.message);
                        $('.alert-danger').show();
                    } else {
                        window.location = '<?php echo site_url("admin/banner"); ?>';
                    }
                },
            });
        });
        $('#frm_login').keydown(function(event) {
            if (event.keyCode == 13) {
                $('#btn_login').trigger('click');
            }
        });
    })
</script>

</html>