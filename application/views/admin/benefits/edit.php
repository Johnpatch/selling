<!-- Left Sidebar Header -->
<?php $this->load->view('admin/header'); ?>
<!-- Left Sidebar Header -->

<!-- Left Sidebar Start -->
<?php $this->load->view('admin/sidebar'); ?>
<!-- Left Sidebar End -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <!--begin::Portlet-->
            <?php if ($this->session->flashdata('error_msg')) {
                echo '<div class="alert alert-danger">' . $this->session->flashdata('error_msg') . '</div>';
            } ?>
            <?php if (isset($error_msg) && $error_msg != '') {
                echo '<div class="alert alert-danger">' . $error_msg . '</div>';
            } ?>
            <?php if ($this->session->flashdata('succ_msg')) {
                echo '<div class="alert alert-success">' . $this->session->flashdata('succ_msg') . '</div>';
            } ?>
            <div class="m-portlet">
                <?php if ($editMode): ?>
                <form class="m-form m-form--fit m-form--label-align-right" method="post"
                  id="edit_airport_fares" name="edit_faq" enctype="multipart/form-data"
                  action="<?php echo site_url('admin/benefits/edit/'.$result['id']); ?>"
                  data-parsley-validate novalidate>
                  <?php else: ?>
                    <form class="m-form m-form--fit m-form--label-align-right" method="post"
                      id="edit_airport_fares" name="edit_faq" enctype="multipart/form-data"
                      action="<?php echo site_url('admin/benefits/create'); ?>"
                      data-parsley-validate novalidate>
                    <?php endif; ?>
                    <?php /** @var \Service\FaqService\Models\FaqInterface $item */ ?>

                    <?php if ($editMode): ?>
                        <input name="id" type="hidden" value="<?php echo $result['id'];?>"/>
                    <?php endif; ?>

					<div class="m-portlet__body">
	                    <div class="form-group m-form__group row">
	                        <label class="col-form-label col-lg-3 col-sm-12">
	                            Description*
	                        </label>
	                        <div class="col-lg-9 col-md-9 col-sm-12">
	                            <textarea name="description" class="form-control" data-provide="markdown" rows="10" required><?php echo $editMode ? $result['description'] : ''; ?></textarea>
	                            <?php echo form_error('description'); ?>
	                        </div>
	                    </div>
	                    
	                    <div class="m-portlet__foot m-portlet__foot--fit">
	                        <div class="m-form__actions m-form__actions">
	                            <div class="row">
	                                <div class="col-lg-9 ml-lg-auto">
	                                    <?php if ($editMode): ?>
	                                    <button 
	                                            class="btn btn-brand"
	                                            type="submit" value="update">Update
	                                    </button>
	                                    <?php else: ?>
	                                    <button 
	                                            class="btn btn-brand"
	                                            type="submit" value="create">Add
	                                    </button>
	                                    <?php endif; ?>
	                                    <a onclick="history.go(-1)"
	                                       class="btn btn-secondary">Go Back</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
                </form>
                <!--end::Form-->
            </div>
        </div>



<?php $this->load->view('admin/footer'); ?>
<script src="<?php echo base_url();?>assets_m/metronic/assets/demo/default/custom/components/forms/validation/form-widgets.js" type="text/javascript"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

<script>
	
</script>