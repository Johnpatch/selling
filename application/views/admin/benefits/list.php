<!-- Left Sidebar Header -->
<?php $this->load->view('admin/header'); ?>
<!-- Left Sidebar Header -->

<!-- Left Sidebar Start -->
<?php $this->load->view('admin/sidebar'); ?>
<!-- Left Sidebar End -->
    
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
        
        <div class="m-content">
            <?php if($this->session->flashdata('error_msg')){
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error_msg').'</div>'; 
            } ?>
            <?php if(isset($error_msg) && $error_msg != ''){
            echo '<div class="alert alert-danger">'.$error_msg.'</div>'; 
            } ?>
            <?php if($this->session->flashdata('succ_msg')){
            echo '<div class="alert alert-success">'.$this->session->flashdata('succ_msg').'</div>'; 
            } ?>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                <!--begin: Search Form -->
                <!--end: Search Form -->
            		<div class="m_datatable1" id="local_data"></div>
                    
                </div>
            </div>
        </div>
<?php $this->load->view('admin/footer'); ?> 

<script>
	var is_first = 0;
	var DatatableJsonProductDemo = function () {
		
		var demo = function () {

			datatable = $('.m_datatable1').mDatatable({
				// datasource definition
				data: {
					type: 'remote',
					source: {
					  read: {
						// sample GET method
						method: 'GET',
						url: '<?php echo site_url();?>admin/benefits/get_list',
						map: function(raw) {
						  // sample data mapping
						  var dataSet = raw;
						  if (typeof raw.data !== 'undefined') {
							dataSet = raw.data;
						  }
						  return dataSet;
						},
					  },
					},
					pageSize: 10,
					serverPaging: true,
					serverFiltering: true,
					serverSorting: true,
				  },

				// layout definition
				layout: {
					scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
					footer: false // display/hide footer
				},

				// column sorting
				sortable: true,
				pagination: true,
				
				toolbar: {
					// toolbar items
					items: {
					  // pagination
					  pagination: {
						// page size select
						pageSizeSelect: [10, 20, 30, 50, 100],
					  },
					},
				  },

				// columns definition
				columns: [{
					field: "title",
					title: "Title",
					width: 100,
					sortable: true,
					template : function(row){
						return row.title;
					}
				}, {
					field: "description",
					title: "Description",
					width: 350,
					sortable: true,
					template : function(row){
						return row.description;
					}
				}, {
					field: "1",
					width: 90,
					title: '',
					sortable: false,
					template: function (row, index, datatable) {
						return '\
							<button type="button" class="btn btn-info" onclick="edit_article('+row.id+')">Edit</button>\
						';
					}
				}]
			});
			
			if(is_first == 0){
				is_first = 1;
				datatable.setDataSourceParam('sort',{});
				datatable.reload();
			}
			
			var query = datatable.getDataSourceQuery();

		};

		return {
			// public functions
			init: function () {
				demo();
			}
		};
	}();
	
	jQuery(document).ready(function () {
		
		
		DatatableJsonProductDemo.init();
	});
	
	function edit_article(id){
		location.href = '<?php echo site_url();?>admin/benefits/edit/'+id;
	}
</script>