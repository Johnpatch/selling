  </div>
</div>
  <footer class="m-grid__item   m-footer ">
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
      <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
        <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
          <span class="m-footer__copyright">
            
          </span>
        </div>
      </div>
    </div>
  </footer>
</div>

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url(); ?>assets_m/js/jquery.min.js"></script> 
        <script src="<?php echo base_url(); ?>assets_m/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/detect.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/fastclick.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/jquery.blockUI.js"></script>
        <!--script src="<?php echo base_url(); ?>assets_m/js/waves.js"></script-->
        <script src="<?php echo base_url(); ?>assets_m/js/wow.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/js/jquery.scrollTo.min.js"></script>

        <script src="<?php echo base_url(); ?>assets_m/js/jquery.core.js"></script>
        <!--script src="<?php echo base_url(); ?>assets_m/js/jquery.app.js"></script-->

        <script src="<?php echo base_url();?>assets_m/metronic/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets_m/metronic/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets_m/metronic/assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>

        <!-- jQuery  -->
        
        <script type="text/javascript" src="<?php echo base_url(); ?>assets_m/plugins/parsleyjs/dist/parsley.min.js"></script>

        <script src="<?php echo base_url(); ?>assets_m/plugins/waypoints/lib/jquery.waypoints.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/plugins/counterup/jquery.counterup.min.js"></script>

        <script src="<?php echo base_url(); ?>assets_m/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_m/plugins/autoNumeric/autoNumeric.js"></script>

        <script src="<?php echo base_url(); ?>assets_m/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js"></script>
        <!-- Sweetalert JS -->
        <script src="<?php echo base_url();?>assets_m/plugins/sweetalert/sweetalert.min.js"></script>
		<script src="<?php echo base_url();?>assets_m/metronic/assets/demo/default/custom/components/forms/widgets/select2.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
            	
                
		        $('#frm_change_password').keydown(function(event) {
		            if (event.keyCode == 13) {
		                $('#btn_change1').trigger('click');
		            }
		        });
	        });
	        
	        function myFunction(){
				return false;
			}
			
			
        </script>
        
        <!-- geocomplete -->
        
        
	</body>
	</html>