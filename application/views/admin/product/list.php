<!-- Left Sidebar Header -->
<?php $this->load->view('admin/header'); ?>
<!-- Left Sidebar Header -->

<!-- Left Sidebar Start -->
<?php $this->load->view('admin/sidebar'); ?>
<!-- Left Sidebar End -->
    
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
        
        <div class="m-content">
            <?php if($this->session->flashdata('error_msg')){
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error_msg').'</div>'; 
            } ?>
            <?php if(isset($error_msg) && $error_msg != ''){
            echo '<div class="alert alert-danger">'.$error_msg.'</div>'; 
            } ?>
            <?php if($this->session->flashdata('succ_msg')){
            echo '<div class="alert alert-success">'.$this->session->flashdata('succ_msg').'</div>'; 
            } ?>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                <!--begin: Search Form -->
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span>
                                                    <i class="la la-search"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                <a href="<?php echo site_url('admin/product/create');?>" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                    <span>
                                        <i class="fa fa-plus"></i>
                                    </span>
                                </a>
                                <div class="m-separator m-separator--dashed d-xl-none"></div>
                            </div>
                        </div>
                    </div>
                <!--end: Search Form -->
            		<div class="m_datatable1" id="local_data"></div>
                    
                </div>
            </div>
        </div>
<?php $this->load->view('admin/footer'); ?> 

<script>
	var is_first = 0;
	var DatatableJsonProductDemo = function () {
		
		var demo = function () {

			datatable = $('.m_datatable1').mDatatable({
				// datasource definition
				data: {
					type: 'remote',
					source: {
					  read: {
						// sample GET method
						method: 'GET',
						url: '<?php echo site_url();?>admin/product/get_list',
						map: function(raw) {
						  // sample data mapping
						  console.log(raw);
						  var dataSet = raw;
						  if (typeof raw.data !== 'undefined') {
							dataSet = raw.data;
						  }
						  return dataSet;
						},
					  },
					},
					pageSize: 10,
					serverPaging: true,
					serverFiltering: true,
					serverSorting: true,
				  },

				// layout definition
				layout: {
					scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
					footer: false // display/hide footer
				},

				// column sorting
				sortable: true,
				pagination: true,
				
				toolbar: {
					// toolbar items
					items: {
					  // pagination
					  pagination: {
						// page size select
						pageSizeSelect: [10, 20, 30, 50, 100],
					  },
					},
				  },

				search: {
					input: $('#generalSearch')
				},

				// columns definition
				columns: [{
					field: "id",
					title: "ID",
					width: 50,
					sortable: false,
					textAlign: 'center',
	        		selector: {class: 'm-checkbox--solid m-checkbox--brand'}
				}, {
					field: "name",
					title: "Product Name",
					width: 150,
					sortable: true,
					template : function(row){
						return row.name;
					}
				}, {
					field: "price",
					title: "Price",
					width: 70,
					sortable: true,
					template: function(row){
						return "$ "+row.price.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
					}
				}, {
					field: "is_show",
					title: "Show？",
					width: 100,
					sortable: true,
					template : function(row){
						if(row.is_show == 0)
							return 'No';
						else
							return 'Yes';
					}
				}, {
					field: "1",
					width: 90,
					title: '',
					sortable: false,
					template: function (row, index, datatable) {
						return '\
							<button type="button" class="btn btn-info" onclick="edit_food('+row.id+')">Edit</button>\
						';
					}
				}, {
					field: "2",
					width: 100,
					title: '<button type="button" class="btn btn-danger" onclick="remove_multi()">Delete</button>',
					sortable: false,
					template: function (row, index, datatable) {
						return '\
							<button type="button" class="btn btn-danger" onclick="remove_food('+row.id+')">Delete</button>\
							';
					}
				}]
			});
			
			if(is_first == 0){
				is_first = 1;
				datatable.setDataSourceParam('sort',{});
				datatable.reload();
			}
			
			var query = datatable.getDataSourceQuery();

		};

		return {
			// public functions
			init: function () {
				demo();
			}
		};
	}();
	
	jQuery(document).ready(function () {
		
		
		DatatableJsonProductDemo.init();
	});
	
	function remove_food(id){
		if(!confirm("Do you want to delete？"))
			return true;
		location.href = '<?php echo site_url();?>admin/product/remove/'+id;
	}
	
	function remove_multi(){
		var multi_checked_index_array = [];
		$('.m-checkbox input:checked').each(function(){
			if(this.value != 'on'){
				multi_checked_index_array.push(this.value);
			}
		});
		if(multi_checked_index_array.length == 0){
			alert("Please select product");
			return true;
		}else{
			if(!confirm("Do you want to delete？"))
				return true;
			$.ajax({
				type: "POST",
				url: "<?php echo site_url();?>admin/product/remove_multi",
				data:{data: multi_checked_index_array},
				success: function(result){
					var obj = JSON.parse(result);
					if(obj['status'] == 1){
						location.href = '<?php echo site_url();?>admin/product';
					}
				}
			});
		}
	}
	
	function edit_food(id){
		location.href = '<?php echo site_url();?>admin/product/edit/'+id;
	}
</script>