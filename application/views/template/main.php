<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">
    <title>Selling</title>
    
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/fonts/icomoon/style.css">

    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/jquery.fancybox.min.css">

    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/aos.css">
    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/style.css">
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
<?php
foreach($this->view_list as $view)
{
    $view_page = $view[0];
    $view_param = $view[1];
    $this->load->view($view_page, $view_param);
}
?>
<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
		<!-- Modal Header -->
		<div class="modal-header">
		  <div style="    width: 30%;text-align: center;margin-top: 10px;">
			<img src="<?php echo base_url();?>assets/images/lock.png" style="width: 48px;"/>
		  </div>
		  <div style="width: 70%;text-align: center;padding-right: 25px;">
			<span id="title"></span><br>
			<span id="price"></span>
		  </div>
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		</div>

		<!-- Modal body -->
		<div class="modal-body">
			<form id="myForm" name="form1" method="POST">
				<div class="form-group">
					<img src="<?php echo base_url();?>assets/images/paypal.png" style="width: 150px;cursor: pointer;"  onclick="paypal()"/>
					<p>or enter your information below</p>
				</div>
				<div class="form-group">
					<div class="row">
						
						<div class="col-md-4">
							<select class="form-control" name="creditCardType">
								<option value="Visa" selected>Visa</option>
								<option value="MasterCard">MasterCard</option>
								<option value="Discover">Discover</option>
								<option value="Amex">American Express</option>
							</select>
						</div>
						<div class="col-md-8">
							<img src="<?php echo base_url();?>assets/images/pay.png" style="width: 100%;"/>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Credit Card Number:</label>
					<input class="form-control" type="text" size="19" maxlength="19" name="creditCardNumber" />
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-8">
							<label>Expirtion Date:</label>
							<div style="display:flex;">
								<select class="form-control col-sm-5" name="expDateMonth">
									<option value="1">01</option>
									<option value="2">02</option>
									<option value="3">03</option>
									<option value="4">04</option>
									<option value="5">05</option>
									<option value="6">06</option>
									<option value="7">07</option>
									<option value="8">08</option>
									<option value="9">09</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
								</select> 
								<select class="form-control col-sm-5 ml-3" name="expDateYear" size="1">
									<?php 
									$cur_year = date('Y');
									
									for($i = $cur_year;$i <= 2030;$i++){
										if($i == $cur_year)
											echo '<option value="'.$cur_year.'" selected>'.$cur_year.'</option>';
										else
											echo '<option value="'.$i.'" >'.$i.'</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<label>CVV:</label>
							<input class="form-control" type="text" size="3" maxlength="4" name="cvv2Number" /><input type="hidden" name="paymentType" value="Sale" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label>First Name:</label>
							<input class="form-control" type="text" name="fname" />
						</div>
						<div class="col-md-6">
							<label>Last Name:</label>
							<input class="form-control" type="text" name="lname" /><input type="hidden" name="ftotal" id="ftotal" value="">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Email:</label>
					<input class="form-control" type="text" name="email" id="email" />
				</div>
				<div class="form-group">
					<input type="checkbox" id="agree"/> I Agree with the <a href="<?php echo site_url('Home/terms');?>">terms of service of AllCertify</a>
				</div>
				<div class="form-group text-center">
					<input type="hidden" name="p_id" id="p_id" />
					<button type="submit" class="btn" style="background: #f16820;color:white;" id="pay_btn"></button>
				</div>
			</form>
		</div>
      </div>
    </div>
</div>
<script src="<?php echo base_url(''); ?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/jquery-migrate-3.0.1.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/jquery.stellar.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/jquery.countdown.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/aos.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/jquery.fancybox.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/jquery.sticky.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/main.js"></script>
<script>
	function purchase(p_id,p_price,p_title){
		var fileter = /^[\w-.+]+@[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/;
		$('#title').html(p_title);
		$('#price').html('Process your order $'+p_price);
		$('#ftotal').val(p_price);
		$('#p_id').val(p_id);
		$('#pay_btn').text('PROCESS YOUR ORDER $'+p_price);
	}
	function paypal(){
		window.location.href = "<?php echo site_url();?>Paypal/create_payment_with_paypal?product_id="+$('#p_id').val()+"&email="+$('#email').val();
	}
	$(document).ready(function(){
		$("form").submit(function(e){
			var val = $('#agree').prop("checked");
			if(val == true){
				e.preventDefault(e);
			    var form_data = $(this).serialize(); 
			    $.ajax({
					url : "<?php echo site_url('Home/paypal_entry');?>",
					type: 'post',
					data : form_data
				}).done(function(response){ //
					var result = JSON.parse(response);
					if(result['result'] == 'fail'){
						alert("Your transaction could not be processed, please review your information and try again.");4371319171114397
					}else{
						window.location.href = "<?php echo site_url('Home/success');?>";
					}
				});
					
			}else{
				alert("Please agree terms of service of AllCertify");
				e.preventDefault(e);
			}
		});
	});
</script>
</body>

</html>
