
  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar py-3 bg-white js-sticky-header site-navbar-target" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
            <a href="<?php echo site_url();?>" class="text-black mb-0"><img src="<?php echo base_url();?>assets/images/logo.png"/></a>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                <li><a href="#home-section" class="nav-link">MAIN</a></li>
                <li><a href="#benefits-section" class="nav-link">BENEFITS</a></li>
                <li><a href="#special-section" class="nav-link">FEATURES</a></li>
                <li><a href="#testimonials-section" class="nav-link">TESTIMONIALS</a></li>
                <li><a href="#blog-section" class="nav-link">ARTICLES</a></li>
                <li><a href="#products-section" class="nav-link">PRODUCTS</a></li>
                <li><a href="#about-section" class="nav-link">ABOUT US</a></li>
              </ul>
            </nav>
          </div>


          <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black float-right"><span class="icon-menu h3"></span></a></div>

        </div>
      </div>
      
    </header>

  
     
    <div class="site-blocks-cover overlay main_section" style="background-image: url(<?php echo base_url();?>assets/images/banner.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center">

          <div class="col-md-12" data-aos="fade-up" data-aos-delay="400">
                        
            <div class="row">
              <div class="col-md-12 banner_section">      
              	<p class="title"><?php echo $main[0]['description']; ?></p>
              	<p class="subtitle"><?php echo $main[1]['description']; ?></p>
                <p class="lead"><?php echo $main[2]['description']; ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
    
    <div class="site-section" id="benefits-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3"><?php echo $benefits[0]['description'];?></h2>
          </div>
        </div>

      <div class="slide-one-item home-slider owl-carousel">
          <div>
            <div class="testimonial">
              <blockquote class="mb-3">
                <p style="font-weight: bold;"><?php echo $benefits[1]['description'];?></p>
                <p><?php echo $benefits[2]['description'];?></p>
              </blockquote>
            </div>
          </div>
      </div>
      </div>
    </div>
    
    <div class="site-section bg-light" id="special-section">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">LEARN ABOUT OUR CERTIFICATION SMART KITS</h2>
          </div>
        </div>
        <div class="row align-items-stretch">
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary icon-pie_chart"></span></div>
              <div>
                <p>Step by step instructions, easy to follow.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary icon-backspace"></span></div>
              <div>
                <p>Affordable</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary icon-av_timer"></span></div>
              <div>
                <p>Instant download</p>
              </div>
            </div>
          </div>


          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="300">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary icon-beenhere"></span></div>
              <div>
                <p>Packed with samples, resources, templates and more. (Save hours and hours of time for a very affordable price)</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="400">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary icon-business_center"></span></div>
              <div>
                <p>Our kits are zero-Risk, the best in the market.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="500">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary icon-cloud_done"></span></div>
              <div>
                <p>MoneyBack Guarantee.</p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    
    
    
    <div class="site-section testimonial-wrap" id="testimonials-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Testimonials</h2>
          </div>
        </div>
      </div>
      <div class="slide-one-item home-slider owl-carousel">
      	<?php foreach($testimonials as $key => $value){ ?>
          <div>
            <div class="testimonial">
              <blockquote class="mb-3">
                <p><?php echo $value['description'];?></p>
              </blockquote>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
    
    <div class="site-section bg-light" id="blog-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Articles</h2>
          </div>
        </div>

        <div class="row">
        	<?php foreach($articles as $key => $value){ ?>
        		
	          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
	            <div class="h-entry">
	              <a href="<?php echo site_url('Home/view_article/'.$value['id']);?>"><h2 style="color: black;" class="font-size-regular"><?php echo $value['title'];?></h2></a>
	              <div class="meta mb-4">Admin <span class="mx-2">&bullet;</span><?php $date = date_create($value['create_date']); echo date_format($date, 'M d, Y');?> </div>
	              <a href="<?php echo site_url('Home/view_article/'.$value['id']);?>"><p class="article_desc"><?php echo $value['description'];?></p></a>
	              <a style="float:right;color: #fd6ea0;" href="<?php echo site_url('Home/view_article/'.$value['id']);?>">read more</a>
	            </div> 
	          </div>
	          	
          <?php } ?>
        </div>
      </div>
    </div>
    
    <div class="site-section" id="products-section">
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-6 text-center">
            <h2 class="section-title mb-3">Our Products</h2>
          </div>
        </div>
        <div class="row">
        	<?php
        	foreach($products as $key => $value){ ?>
	          <div class="col-lg-4 col-md-6 mb-5 product_box">
	            <div class="product-item">
	              <div class="px-4">
	                <h3 style="font-weight: bold;" class="product_title"><?php echo $value['name'];?></h3>
	                <p class="mb-4"><?php echo $value['description'];?></p>
	                <p style="font-weight: bold;">No risk. Money back guarantee. </p>
	                <p style="font-weight: bold;" class="product_price">Price: $<?php echo $value['price'];?></p>
	                <div>
	                  <button onclick="purchase(<?php echo $value['id'];?>,<?php echo $value['price'];?>,'<?php echo $value['name'];?>')" type="button" class="btn btn-black mr-1 mt-2 rounded-0" data-toggle="modal" data-target="#myModal">Purchase</button>
	                </div>
	              </div>
	            </div>
	          </div>
			<?php } ?>
        </div>
      </div>
    </div>

    <div class="site-section bg-light" id="about-section">
      <div class="container">
        <div class="row align-items-lg-center">
          <!--div class="col-md-8 mb-5 mb-lg-0 position-relative">
            <img src="<?php echo base_url();?>assets/images/about_1.jpg" class="img-fluid" alt="Image">
          </div-->
          <div class="col-md-12 ml-auto">
            <h2 class="section-title mb-3"><?php echo $about[0]['description']; ?></h2>
            <p class="mb-4"><?php echo $about[1]['description']; ?></p>
          </div>
        </div>
      </div>
    </div>

  </div> <!-- .site-wrap -->