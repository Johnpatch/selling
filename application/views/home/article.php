
  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar py-3 bg-white js-sticky-header site-navbar-target" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
            <a href="<?php echo site_url();?>" class="text-black mb-0"><img src="<?php echo base_url();?>assets/images/logo.png"/></a>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                <li><a href="#home-section" class="nav-link">MAIN</a></li>
                <li><a href="#benefits-section" class="nav-link">BENEFITS</a></li>
                <li><a href="#special-section" class="nav-link">FEATURES</a></li>
                <li><a href="#products-section" class="nav-link">PRODUCTS</a></li>
                <li><a href="#testimonials-section" class="nav-link">TESTIMONIALS</a></li>
                <li><a href="#blog-section" class="nav-link">ARTICLES</a></li>
                <li><a href="#about-section" class="nav-link">ABOUT US</a></li>
              </ul>
            </nav>
          </div>


          <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black float-right"><span class="icon-menu h3"></span></a></div>

        </div>
      </div>
      
    </header>


    
    <div class="site-section" id="blog-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Articles</h2>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="h-entry">
              <h2 style="color: black;" class="font-size-regular"><?php echo $article['title'];?></h2>
              <div class="meta mb-4">Admin <span class="mx-2">&bullet;</span><?php $date = date_create($article['create_date']); echo date_format($date, 'M d, Y');?> </div>
              <p ><?php echo $article['description'];?></p>
            </div> 
          </div>
        </div>
        <a href="<?php echo site_url();?>">Go To Hompage</a>
      </div>
    </div>
    



  </div> <!-- .site-wrap -->