<?php

class Common extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        
    }

    /*
     * Get menu action
     */
    function get_menu($menu)
    {
        $menuname= "";
        $filename = $this->uri->segment(2);
        if($filename == $menu)
        {
            $menuname = "active";
        }
        else
        {
            $menuname = "has-submenu";
        }
        return  $menuname;
    }
    
    /*
     * Check login authentication
     */
    function login($email, $password)
	{
	    // Login with md5 hash first
        $md5PasswordHash = PasswordHashFactory::create($password, PasswordHashFactory::MD5);
        $this->db->select('*');
        $this->db->from('tbl_admin_details');
        $this->db->where('email', $email);
        $this->db->where('password', $md5PasswordHash);
        $this->db->where('is_active',1);
        $this->db->limit(1);
        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            $admin = $query->row_array();

            // Replace md5 password hash with a secure hash
            $secureHash = PasswordHashFactory::create($password);
            $this->db->where('id',$admin['id']);
            $this->db->update('tbl_admin_details',[
                'password' => $secureHash
            ]);
            return $admin;
        } else {
            $this->db->select('*');
            $this->db->from('tbl_admin_details');
            $this->db->where('email', $email);
            $this->db->where('is_active',1);
            $this->db->limit(1);
            $query = $this->db->get();

            $admin = $query->row_array();

            if(password_verify($password, $admin['password'])) {
                return $admin;
            }
        }
        return false;
	}

    function sendSMS($phone, $message)
    {
        $id = TWILIO_SID;
        $token = TWILIO_TOKEN;
        $url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
        $data = array (
            'From' => "+61428924988",
            'To' => $phone,
            'Body' => $message,
        );
        $post = http_build_query($data);
        $x = curl_init($url );
        curl_setopt($x, CURLOPT_POST, true);
        curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
        curl_setopt($x, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($x);
        curl_close($x);
    }

    /*
     * Get country list by id
     */
    function get_countrylist()
    {
        return $this->db->get_where('tbl_country',array('is_active'=>'1'))->result_array();
    }
    
    /*
     * Get admin detail by id
     */
    function get_admin($id)
    {
        return $this->db->get_where('tbl_admin_details',array('id'=>$id))->row_array();
    }

    /*
     * function to update tbl_admin_details
     */
    function update_admin($id,$params)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_admin_details',$params);
    }

    /*
     * Upload image and thumb images on specific path
    */
    function uploadImage($files, $filename, $upload_path)
    {
        if (!empty($files['name']) && $files['size'] > 0) 
        {
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['file_name'] = random_string('numeric', 5).strtotime(date("Ymd his"));
            // $config['max_size'] = '2048KB';
            // $config['max_width']  = '2000';
            // $config['max_height']  = '2000';

            //$this->upload->initialize($config);
            $this->load->library('upload', $config);
            if($this->upload->do_upload($filename))
            {   
                $w = $this->upload->data();
                $uploaded_image = $w['file_name'];
                $config = array(
                'image_library'  => 'gd2',
                'new_image'      => $upload_path."thumb/",
                'source_image'   => $upload_path.$w['file_name'],
                'create_thumb'   => false,    
                'width'          => "100",
                'height'         => "100",
                'maintain_ratio' => TRUE,
                );
                $this->load->library('image_lib'); // add liberary
                $this->image_lib->initialize($config);
                $this->image_lib->resize();

                return $uploaded_image;
            }
            else
            {
                return array('status' => false, 'error' => $this->upload->display_errors());
            }
        }
    }

    /*
     * Upload edit image and thumb images on specific path
    */
    function uploadEditImage($files, $filename, $old_profile_image, $upload_path)
    {   
        if (!empty($files['name']) && $files['size'] > 0) 
        {
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
            $config['file_name'] = random_string('numeric', 5).strtotime(date("Ymd his"));

            $this->upload->initialize($config);

            if($this->upload->do_upload($filename))
            {   
                //Remove old file
                if(file_exists($upload_path.$old_profile_image) && $old_profile_image != "default.jpeg" && !empty($old_profile_image)) {
                    $this->removeImage($old_profile_image, $upload_path);
                }

                $w = $this->upload->data();
                $uploaded_image = $w['file_name'];

                $file = $upload_path.$w['file_name'];
                $extension = pathinfo($file, PATHINFO_EXTENSION);

                if(!in_array($extension, ['gif', 'jpeg', 'png', 'jpg'])) {
                    // No need to resize non-image types
                    return $uploaded_image;
                }

                // Resize the original image
                $data = ImageSizeCalculatorTrait::calculateBestTargetSizeForImageResize($file);

                log_message('error', "[v2] Calculated image resize data is " . var_export($data, true));

                $image = new ImageResize($file);
                $image->save($file);

                // Create a thumb of the original image
                $data = ImageSizeCalculatorTrait::calculateBestTargetSizeForImageResize($file, true);

                log_message('error', "[v2] Calculated thumbnail resize data is " . var_export($data, true));

                $thumb = new ImageResize($file);
                $thumb->resizeToBestFit($data['targetWidth'], $data['targetHeight']);

                log_message('error', '[v2] Thumb name is ' . "{$upload_path}thumb/{$config['file_name']}.{$extension}");

                $thumb->save("{$upload_path}thumb/{$config['file_name']}.{$extension}");

                return $uploaded_image;
            }
            else
            {
                return array('status' => false, 'error' => $this->upload->display_errors());
            }
        }
    }

    /*
     * Get setings details
    */
    function getSettingsdetails($user_type)
    {
        $this->db->select('*');
        $this->db->from('tbl_settings');
        $this->db->where('type', $user_type);
        $query = $this->db->get();      

        if($query->num_rows() > 0)
        {   
            $settings_data = array();
            foreach ($query->result_array() as $settingskey => $settingsvalue) {
                $settings_data[$settingsvalue['meta_key']] = $settingsvalue['meta_value'];
            }
            return $settings_data;
        }
        else
        {
            return false;
        }
    }

    /*
     * function to update tbl_settings
     */
    function update_settings($type, $meta_key,$meta_value)
    {
        $this->db->where('type',$type);
        $this->db->where('meta_key',$meta_key);
        $this->db->update('tbl_settings',array("meta_value" => $meta_value));
    }

    /*
     * Get near all free driver list
    */
    function getNeardriverlist($car_type, $latitude, $longitude, $company)
    {
        //Get distance details
        $settings = $this->getSettingsdetails('driver');
        $sql = "SELECT *, 6371 * 2 * ASIN(SQRT( POWER(SIN(($latitude - latitude) * pi()/180 / 2), 2) + COS($latitude * pi()/180) * COS(latitude * pi()/180) *POWER(SIN(($longitude - longitude) * pi()/180 / 2), 2) )) as distance FROM tbl_driver WHERE company = '".$company."' AND id IN (SELECT driver_id from tbl_driver_cartype WHERE car_type = '".$car_type."') AND is_service = 1 AND is_free = 1 AND is_login = 1 AND is_active = 1 HAVING distance <= ".$settings['base_distance']." ORDER by distance ";
        
        $query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {   
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    /*
     * Date Time Conversion
    */
    function GmtTimeToLocalTime($time) {
        $date =  new DateTime(date('Y-m-d h:i:s',$time),new DateTimezone('UTC'));
        $date->setTimezone(new \DateTimezone('Asia/Calcutta'));
        return $date->format("Y-m-d H:i:s");
    }

    /*
     * get full address by lat long
    */
    function get_lat_long($address){

        $address = str_replace(" ", "+", $address);

        $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&key=".getenv('GOOGLE_MAPS_KEY'));
        $json = json_decode($json);
        $lat = $long = 0;
        if(count($json->results) > 0)
        {
            $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
        }
        return $lat.','.$long;
    }
    
    /*
     * Remove org & thumb image on specific path
    */
    function removeImage($image, $path)
    { 
        //Remove uploaded images
        if(file_exists($path.$image) && $image != 'default.jpeg' && !empty($image))
        {
            unlink($path.$image);

            if(file_exists($path.'thumb/'.$image)) {
                unlink($path.'thumb/'.$image);
            }
        }
    }

    //POST curl calling
    function post_curl_call($url, $params, $headers)
    {
        //url-ify the data for the POST
        $params_value = '';
        foreach($params as $key=>$value) { $params_value .= $key.'='.$value.'&'; }
        $params_value = rtrim($params_value, '&');

        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //Set method post
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        //Set post parametersparams
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_value);
        //set headers
        curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
        // $response contains the response string
        $response = json_decode(curl_exec($ch));
        // close curl resource to free up system resources
        $err = curl_error($ch);
        curl_close($ch); 
        if ($err) {
          return $err;
        } else {
          return $this->object_to_array($response);
        }
    }

    //PUT curl calling
    function put_curl_call($url, $params, $headers)
    {
        //url-ify the data for the POST
        $params_value = '';
        foreach($params as $key=>$value) { $params_value .= $key.'='.$value.'&'; }
        $params_value = rtrim($params_value, '&');

        // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //Set method post
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        //Set post parametersparams
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_value);
        //set headers
        curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
        // $response contains the response string
        $response = json_decode(curl_exec($ch));
        // close curl resource to free up system resources
        $err = curl_error($ch);
        curl_close($ch); 
        if ($err) {
          return $err;
        } else {
          return $this->object_to_array($response);
        }
    }

    function object_to_array($obj) {
      if(is_object($obj)) $obj = (array) $obj;
      if(is_array($obj)) {
          $new = array();
          foreach($obj as $key => $val) {
              $new[$key] = $this->object_to_array($val);
          }
      }
      else $new = $obj;
      return $new;       
    }
    
    //Date convert on specific timezone.
    function date_convert($date, $timezone, $dateformat) {
        if($date == '0000-00-00 00:00:00')
            return $date;
        $date = new DateTime($date);
        $date->setTimezone(new DateTimeZone($timezone));

        return $date->format($dateformat);
    }

    /*
     * For Driver GCM Push Notification
    */
    function send_gcm_notification_driver($registatoin_ids, $message){
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';
        
        $fields = array(
            'registration_ids' => array($registatoin_ids),
            'data' => array("message" => $message),
            'time_to_live' => 5400
        );

        $headers = array(
            'Authorization: key=AIzaSyCNOnvAdFY1uEgdnZzn95Sc0axzVrsKQy8',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        //echo "+++++++++++++++||||";
        //var_dump($result);
        return;         
    }

    /*
     * For user GCM Push Notification
    */
    function send_gcm_notification_user($registatoin_ids, $message){
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';
        
        $fields = array(
            'registration_ids' => array($registatoin_ids),
            'data' => array("message" => $message),
            'time_to_live' => 5400
        );

        $headers = array(
            'Authorization: key=AIzaSyDTSqlMbqtOFSlpFKWHWCyLGwLZgKZuxds',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        //echo "+++++++++++++++||||";
        //var_dump($result);
        return;         
    }

    /*
     * For Driver IOS Push Notification
    */
    function send_notification_ios_driver($payload,$device_tokens)
    {   
        $development = true;
        $Production = true;
        $payload = json_encode($payload);
        
        $apns_url = NULL;
        $apns_cert = NULL;
        $apns_port = 2195;
        
        if($development)
        {
            $apns_url = 'gateway.sandbox.push.apple.com';
            $apns_cert = './assets/pem/Driver_Dev_APNS.pem';
        }
        
        $stream_context = stream_context_create();
        stream_context_set_option($stream_context, 'ssl', 'local_cert', $apns_cert);
        stream_context_set_option($stream_context, 'ssl', 'passphrase',"hyperlink");
        
        $apns = stream_socket_client('ssl://' . $apns_url . ':' . $apns_port, $error, $error_string, 2, STREAM_CLIENT_CONNECT, $stream_context);
        
        if (!$apns) {
            print "Failed to connect $err $errstr\n";
            //exit;
            $success_connection = 0;
        } else {
            //echo "ok";
            $success_connection = 1;
        }

        if($device_tokens)
        {
            $apns_message = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device_tokens)) . chr(0) . chr(strlen($payload)) . $payload;
            fwrite($apns, $apns_message);
            //var_dump($apns_message);
        }
        
        /*----------Production--------------*/
        if($Production)
        {
            $apns_url1 = 'gateway.push.apple.com';
            $apns_cert1 = './assets/pem/Driver_Live_APNS.pem';
        }
        $stream_context1 = stream_context_create();
        stream_context_set_option($stream_context1, 'ssl', 'local_cert', $apns_cert1);
        stream_context_set_option($stream_context1, 'ssl', 'passphrase',"hyperlink");
        
        $apns1 = stream_socket_client('ssl://' . $apns_url1 . ':' . $apns_port, $error, $error_string, 2, STREAM_CLIENT_CONNECT, $stream_context1);
        
        if (!$apns1) {
            print "Failed to connect $err $errstr\n";
            //exit;
            $success_connection = 0;
        } else {
            //echo "ok";
            $success_connection = 1;
        }
        

        if($device_tokens )
        {
            $apns_message = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device_tokens)) . chr(0) . chr(strlen($payload)) . $payload;
            fwrite($apns1, $apns_message);
            //var_dump($apns_message);
        }
        
        @socket_close($apns);
        @fclose($apns);
        return;
        // END CODE FOR PUSH NOTIFICATIONS TO ALL USERS
    }

    /*
     * For User IOS Push Notification
    */
    function send_notification_ios_user($payload,$device_tokens)
    {   
        $development = true;
        $Production = true;
        $payload = json_encode($payload);
        
        $apns_url = NULL;
        $apns_cert = NULL;
        $apns_port = 2195;
        
        if($development)
        {
            $apns_url = 'gateway.sandbox.push.apple.com';
            $apns_cert = './assets/pem/Member_Dev_APNS.pem';
        }

        $stream_context = stream_context_create();
        stream_context_set_option($stream_context, 'ssl', 'local_cert', $apns_cert);
        stream_context_set_option($stream_context, 'ssl', 'passphrase',"hyperlink");
       
        $apns = stream_socket_client('ssl://' . $apns_url . ':' . $apns_port, $error, $error_string, 2, STREAM_CLIENT_CONNECT, $stream_context);
        
        if (!$apns) {
            print "Failed to connect $err $errstr\n";
            //exit;
            $success_connection = 0;
        } else {
            //echo "ok";
            $success_connection = 1;
        }

        if($device_tokens )
        {
            $apns_message = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device_tokens)) . chr(0) . chr(strlen($payload)) . $payload;
            fwrite($apns, $apns_message);
            //var_dump($apns_message);
        }
        /*----------Production--------------*/
        if($Production)
        {
            $apns_url1 = 'gateway.push.apple.com';
            $apns_cert1 = './assets/pem/Member_Live_APNS.pem';
        }
        $stream_context1 = stream_context_create();
        stream_context_set_option($stream_context1, 'ssl', 'local_cert', $apns_cert1);
        stream_context_set_option($stream_context1, 'ssl', 'passphrase',"hyperlink");
        
        $apns1 = stream_socket_client('ssl://' . $apns_url1 . ':' . $apns_port, $error, $error_string, 2, STREAM_CLIENT_CONNECT, $stream_context1);
        
        if (!$apns1) {
            print "Failed to connect $err $errstr\n";
            //exit;
            $success_connection = 0;
        } else {
            //echo "ok";
            $success_connection = 1;
        }
        

        if($device_tokens )
        {
            $apns_message = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device_tokens)) . chr(0) . chr(strlen($payload)) . $payload;
            fwrite($apns1, $apns_message);
            //var_dump($apns_message);
        }
        
        @socket_close($apns);
        @fclose($apns);
        return;
        // END CODE FOR PUSH NOTIFICATIONS TO ALL USERS
    }
    
    function upload($files, $filename, $upload_path)
    {
        if (!empty($files['name']) && $files['size'] > 0) 
        {
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 0;

            $this->load->library('upload', $config);
            if($this->upload->do_upload($filename))
            {   
                return true;
            }
            else
            {
                return array('status' => false, 'error' => $this->upload->display_errors());
            }
        }
    }
}
/*End class Common ends*/
?>