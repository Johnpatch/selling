<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Model extends CI_Model {

	public function get_data($table,$where = array(), $field = '', $sort = ''){
		$this->db->select('*');
        $this->db->from($table);
        $this->db->where('is_delete' , '0');
        $this->db->where($where);
        if($field != '' && $sort != ''){
			$this->db->order_by($field, $sort);
		}
        return $this->db->get()->result_array();
	}   
	
	function login($email, $password)
	{
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('email', $email);
        $this->db->where('password', sha1($password));
        $this->db->where('active',1);
        $this->db->where('user_type',1);
        $this->db->limit(1);
        return $this->db->get()->row_array();
	}
	
	function insert($table_name, $data){
		$this->db->insert($table_name, $data);
		return $this->db->insert_id();
	}
	
	function update($table_name, $data){
		$this->db->where('id',$data['id']);
		$this->db->update($table_name, $data);
		return true;
	}
	
	function delete($table_name, $id){
		return $this->db->delete($table_name, array('id'=>$id));
	}

	public function get_item($table,$where = array()){
		$this->db->select('*');
        $this->db->from($table);
        $this->db->where('is_delete' , '0');
        $this->db->where($where);
        return $this->db->get()->row_array();
	} 	
	
	function get_products(){
		$sql = "SELECT B.name as category_name, A.* FROM tb_product A INNER JOIN tb_category B ON A.category = B.id";
		return $this->db->query($sql)->result_array();
	}
	
	function get_admin_info(){
		$sql = "SELECT * FROM tb_user WHERE active = 1 and user_type = 1";
		return $this->db->query($sql)->row_array();
	}
	
	function update_admin_password($new_password){
		$data['password'] = sha1($new_password);
		$this->db->where('id',$this->session->userdata('id'));
		$this->db->where('active',1);
		$this->db->where('user_type',1);
		$this->db->update('tb_user',$data);
		return true;
	}
	
	function get_deliver_count($type,$search = '', $date = ''){
		if($search == '' && $date == ''){
			$this->db->from('tb_order');
			$this->db->where('status',$type);
			return $this->db->count_all_results();	
		}else{
			$sql = "SELECT * FROM tb_order WHERE ";
			$sql .= " ( concat(order_date,id) LIKE '%".$search."%' OR address LIKE '%".$search."%' OR email LIKE '%".$search."%' OR phone LIKE '%".$search."%' OR concat(firstname,' ',lastname) LIKE '%".$search."%'";
			$search_arr = explode(' ',trim($search));
			if(!empty($search_arr[1])){
				foreach($search_arr as $key => $value){
					$sql .= " OR ( concat(order_date,id) LIKE '%".$value."%' OR address LIKE '%".$value."%' OR email LIKE '%".$value."%' OR phone LIKE '%".$value."%' OR concat(firstname,' ',lastname) LIKE '%".$value."%') ";
				}
			}
			
			$sql .= " ) ";
			if($date != ''){
				$date_arr = explode(" - ",$date);
				$start_date = date("Y-m-d", strtotime($date_arr[0]));
				$end_date = date("Y-m-d", strtotime($date_arr[1]));
				$start_date .= " 00:00:00";
				$end_date .= " 23:59:59";
				$sql .= " and expect_date >= '".$start_date."' and expect_date <= '".$end_date."'";
			}
			$sql .= " and status = ".$type;
			$query = $this->db->query($sql);
			if($query->num_rows() > 0)
				return $query->num_rows();
			else
				return false;
		}
	}
	
	function list_deliver($status, $limit,$per_page,$field = "id",$sort="desc",$search,$date){
		$sql = "SELECT *, CONCAT(firstname, ' ', lastname) as name FROM tb_order WHERE status = ".$status." and is_delete = 0 ";
		if($search != ''){
			$sql .= " AND (concat(order_date,id) LIKE '%".$search."%' OR address LIKE '%".$search."%' OR email LIKE '%".$search."%' OR phone LIKE '%".$search."%' OR concat(firstname,' ',lastname) LIKE '%".$search."%' ";
			$search_arr = explode(' ',trim($search));
			if(!empty($search_arr[1])){
				foreach($search_arr as $key => $value){
					$sql .= " OR ( concat(order_date,id) LIKE '%".$value."%' OR address LIKE '%".$value."%' OR email LIKE '%".$value."%' OR phone LIKE '%".$value."%' OR concat(firstname,' ',lastname) LIKE '%".$value."%') ";
				}
			}
			$sql .= " ) ";
		}
		if($date != ''){
			$date_arr = explode(" - ",$date);
			$start_date = date("Y-m-d", strtotime($date_arr[0]));
			$end_date = date("Y-m-d", strtotime($date_arr[1]));
			$start_date .= " 00:00:00";
			$end_date .= " 23:59:59";
			$sql .= " and expect_date >= '".$start_date."' and expect_date <= '".$end_date."'";
		}
		$sql .= "ORDER BY ".$field." ".$sort." LIMIT ".$limit." , ".$per_page;
		$query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}
	
	function deliver($id,$type){
		$data['status'] = $type;
		$this->db->where('unique',$id);
		$this->db->update('tb_order',$data);
		return true;
	}
	
	function get_order_details($id){
		$sql = "SELECT A.*, C.name,C.image,C.price FROM tb_order_detail A LEFT JOIN tb_order B ON B.id = A.order_id and B.order_date = A.order_date INNER JOIN tb_product C on C.id = A.food_id WHERE B.unique = ".$id;
		return $this->db->query($sql)->result_array();
	}
	
	function get_history_details($id){
		$sql = "SELECT * FROM tb_order where user_id = ".$id;
		return $this->db->query($sql)->result_array();
	}
	
	function get_product_count($search = '', $category = ''){
		if($search == '' && $category == ''){
			$this->db->from('tb_product');
			return $this->db->count_all_results();	
		}else{
			$sql = "SELECT A.* FROM tb_product A WHERE ";
			$sql .= " ( A.name LIKE '%".$search."%' OR A.price LIKE '%".$search."%' ";
			$search_arr = explode(' ',trim($search));
			if(!empty($search_arr[1])){
				foreach($search_arr as $key => $value){
					$sql .= " OR ( A.name LIKE '%".$value."%' OR A.price LIKE '%".$value."%') ";
				}
			}
			
			$sql .= " ) ";
			if($category != ''){
				$sql .= " and A.category = ".$category;
			}
			$query = $this->db->query($sql);
			if($query->num_rows() > 0)
				return $query->num_rows();
			else
				return false;
		}
	}
	
	function list_product($limit,$per_page,$field = "id",$sort="desc",$search,$category){
		$sql = "SELECT A.* FROM tb_product A WHERE A.is_delete = 0 ";
		if($search != ''){
			$sql .= " AND (A.name LIKE '%".$search."%' OR A.price LIKE '%".$search."%' ";
			$search_arr = explode(' ',trim($search));
			if(!empty($search_arr[1])){
				foreach($search_arr as $key => $value){
					$sql .= " OR ( A.name LIKE '%".$value."%' OR A.price LIKE '%".$value."%') ";
				}
			}
			$sql .= " ) ";
		}
		if($category != ''){
			$sql .= " and A.category = ".$category;
		}
		if($field != 'email'){
			
			$sql .= " ORDER BY A.".$field." ".$sort." LIMIT ".$limit." , ".$per_page;
			
		}
		
		$query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}
	
	function remove_category_products($id){
		$data['category'] = '';
		$this->db->where('category',$id);
		$this->db->update('tb_product', $data);
		return true;
	}
	
	function get_cur_image_name($id){
		$sql = "SELECT image from tb_product where id = ".$id;
		$result = $this->db->query($sql)->row_array();
		return $result['image'];
	}
	
	function get_inquiry_count($search = '', $date = '', $user = '', $food = '',$type){
		if($search == '' && $date == '' && $user == '' && $food == ''){
			$this->db->from('tb_inquiry');
			$this->db->where('is_delete',0);
			if($type == 0)
				$this->db->where('price is NULL');
			else
				$this->db->where('price is NOT NULL');
			return $this->db->count_all_results();	
		}else{
			$sql = "SELECT A.*,B.firstname, B.lastname,B.email, C.image,C.name FROM tb_inquiry A LEFT JOIN tb_user B ON A.user_id = B.id LEFT JOIN tb_product C ON A.product_id = C.id WHERE ";
			$sql .= " ( concat(B.firstname,B.lastname) LIKE '%".$search."%' OR B.email LIKE '%".$search."%' ";
			$search_arr = explode(' ',trim($search));
			if(!empty($search_arr[1])){
				foreach($search_arr as $key => $value){
					$sql .= " OR ( concat(B.firstname, ' ',B.lastname) LIKE '%".$value."%' OR B.email LIKE '%".$value."%' )";
				}
			}
			
			$sql .= " ) ";
			if($date != ''){
				$date_arr = explode(" - ",$date);
				$start_date = date("Y-m-d", strtotime($date_arr[0]));
				$end_date = date("Y-m-d", strtotime($date_arr[1]));
				$start_date .= " 00:00:00";
				$end_date .= " 23:59:59";
				$sql .= " and A.create_date >= '".$start_date."' and A.create_date <= '".$end_date."'";
			}
			if($user != ''){
				$sql .= " and A.user_id = ".$user." ";
			}
			if($food != ''){
				$sql .= " and A.product_id = ".$food." ";
			}
			$sql .= " AND A.is_delete = 0 ";
			if($type == 0)
				$sql .= " AND A.price IS NULL ";
			else
				$sql .= " AND A.price IS NOT NULL ";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0)
				return $query->num_rows();
			else
				return 0;
		}
	}
	
	function list_inquiry($limit,$per_page,$field = "id",$sort="desc",$search,$date,$user,$food,$type){
		$sql = "SELECT A.*, CONCAT(B.firstname, ' ', B.lastname) as name, B.email, C.image, C.name as food_name FROM tb_inquiry A LEFT JOIN tb_user B ON A.user_id = B.id LEFT JOIN tb_product C ON A.product_id = C.id WHERE A.is_delete = 0 ";
		if($search != ''){
			$sql .= " AND (concat(B.firstname, ' ',B.lastname) LIKE '%".$search."%' OR B.email LIKE '%".$search."%' ";
			$search_arr = explode(' ',trim($search));
			if(!empty($search_arr[1])){
				foreach($search_arr as $key => $value){
					$sql .= " OR ( concat(B.firstname, ' ',B.lastname) LIKE '%".$value."%' OR B.email LIKE '%".$value."%') ";
				}
			}
			$sql .= " ) ";
		}
		if($type == 0)
			$sql .= " AND A.price IS NULL ";
		else
			$sql .= " AND A.price IS NOT NULL ";
		if($date != ''){
			$date_arr = explode(" - ",$date);
			$start_date = date("Y-m-d", strtotime($date_arr[0]));
			$end_date = date("Y-m-d", strtotime($date_arr[1]));
			$start_date .= " 00:00:00";
			$end_date .= " 23:59:59";
			$sql .= " and A.create_date >= '".$start_date."' and A.create_date <= '".$end_date."'";
		}
		if($user != ''){
			$sql .= " and A.user_id = ".$user." ";
		}
		if($food != ''){
			$sql .= " and A.product_id = ".$food." ";
		}
		if($field == 'name')
			$sql .= "ORDER BY B.".$field." ".$sort." LIMIT ".$limit." , ".$per_page;
		else if($field == 'id' || $field == 'create_date')
			$sql .= "ORDER BY A.".$field." ".$sort." LIMIT ".$limit." , ".$per_page;
		else
			$sql .= "ORDER BY B.".$field." ".$sort." LIMIT ".$limit." , ".$per_page;
		$query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}
	
	function get_user_list(){
		$this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('is_delete' , '0');
        $this->db->where('active',1);
        $this->db->where('user_type',3);
        return $this->db->get()->result_array();
	}
	
	function get_food_list(){
		$this->db->select('*');
        $this->db->from('tb_product');
        $this->db->where('is_delete' , '0');
        $this->db->where('is_show',1);
        return $this->db->get()->result_array();
	}
	
	function get_price_list(){
		$this->db->select('*');
        $this->db->from('tb_inquiry');
        $this->db->where('is_delete' , '0');
        $this->db->where('price IS NULL');
        return $this->db->get()->result_array();
	}
	
	function get_original_price($id){
		$this->db->select('product_id');
		$this->db->from('tb_inquiry');
		$this->db->where('is_delete' , '0');
		$this->db->where('id',$id);
        $result = $this->db->get()->row_array();
        $product_id = $result['product_id'];
        
		$this->db->select('price');
		$this->db->from('tb_product');
		$this->db->where('is_delete' , '0');
        $this->db->where('is_show',1);
        $this->db->where('id',$product_id);
        $result = $this->db->get()->row_array();
        return $result['price'];
	}
	
	function get_changed_price($id){
		$this->db->select('price');
		$this->db->from('tb_inquiry');
		$this->db->where('is_delete' , '0');
		$this->db->where('id',$id);
        $result = $this->db->get()->row_array();
        return $result['price'];
	}
	
	function get_article_count($search = ''){
		if($search == ''){
			$this->db->from('tb_articles');
			return $this->db->count_all_results();	
		}else{
			$sql = "SELECT A.* FROM tb_product A WHERE ";
			$sql .= " ( A.title LIKE '%".$search."%' ";
			$search_arr = explode(' ',trim($search));
			if(!empty($search_arr[1])){
				foreach($search_arr as $key => $value){
					$sql .= " OR ( A.title LIKE '%".$value."%') ";
				}
			}
			
			$sql .= " ) ";
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0)
				return $query->num_rows();
			else
				return false;
		}
	}
	
	function list_article($limit,$per_page,$field = "id",$sort="desc",$search){
		$sql = "SELECT A.* FROM tb_articles A WHERE A.is_delete = 0 ";
		if($search != ''){
			$sql .= " AND (A.title LIKE '%".$search."%' ";
			$search_arr = explode(' ',trim($search));
			if(!empty($search_arr[1])){
				foreach($search_arr as $key => $value){
					$sql .= " OR ( A.title LIKE '%".$value."%') ";
				}
			}
			$sql .= " ) ";
		}	
		$sql .= " ORDER BY A.".$field." ".$sort." LIMIT ".$limit." , ".$per_page;
		
		$query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}
	
	function get_section_count($table_name){
		$this->db->from($table_name);
		return $this->db->count_all_results();	
	}
	
	function list_section($limit,$per_page,$field = "id",$sort="desc",$table_name){
		$sql = "SELECT A.* FROM ".$table_name." A WHERE A.is_delete = 0 ";
		
		$sql .= " ORDER BY A.".$field." ".$sort." LIMIT ".$limit." , ".$per_page;
		
		$query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}
}
