<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_Model extends Web_Model {

    function get_data($table,$where = array(), $field = '', $sort = '', $cur_page = 0, $per_page = 0){
		$this->db->select('*');
        $this->db->from($table);
        $this->db->where('is_delete' , '0');
        $this->db->where($where);
        if($field != '' && $sort != ''){
			$this->db->order_by($field, $sort);
		}
		if($cur_page != 0 && $per_page != 0)
			$this->db->limit($per_page, ($cur_page-1)*$per_page);
		$query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
	}   
	
	function get_articles($table,$where = array(), $field = '', $sort = '', $cur_page = 0, $per_page = 0){
		$this->db->select('*');
        $this->db->from($table);
        $this->db->where('is_delete' , '0');
        $this->db->where($where);
        $this->db->where("create_date > DATE_SUB(now(), INTERVAL 6 MONTH)");
        if($field != '' && $sort != ''){
			$this->db->order_by($field, $sort);
		}
		if($cur_page != 0 && $per_page != 0)
			$this->db->limit($per_page, ($cur_page-1)*$per_page);
		$query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
            return array();
	}   

	function get_new_home(){
		$this->db->select('*');
		$this->db->from('tb_news');
		$this->db->where('is_delete' , '0');
		$this->db->where('is_show' , '1');
		$this->db->order_by('date','desc');
		$this->db->limit(3);
		return $this->db->get()->result_array();
	}
	
	public function get_item($table,$where = array()){
		$this->db->select('*');
        $this->db->from($table);
        $this->db->where('is_delete' , '0');
        $this->db->where($where);
        return $this->db->get()->row_array();
	} 
	
	function get_count($table, $where){
		$this->db->select('*');
        $this->db->from($table);
        $this->db->where('is_delete' , '0');
        $this->db->where($where);
        $result = $this->db->get()->result_array();
        return count($result);
	}
	
	function get_products($category, $cur_page){
		$sql = "SELECT * FROM tb_product WHERE ";
		if($category != 'all')
			$sql .= " category = ".$category;
		else
			$sql .= " category != ''";
		$sql .= " and is_show = 1 and is_delete = 0 ";
		$sql .= " order by id desc ";
		$sql .= " limit ".$cur_page.", 4";
		$result = $this->db->query($sql)->result_array();
		return $result;
	}

	function get_total($category){
		$sql = "SELECT * FROM tb_product WHERE ";
		if($category != 'all')
			$sql .= " category = ".$category;
		else
			$sql .= " category != ''";
		$sql .= " and is_show = 1 and is_delete = 0 ";
		$result = $this->db->query($sql)->result_array();
		return count($result);
	}	
	
	function insert_new_address($address,$userId){
		$data['address'] = $address;
		$data['user_id'] = $userId;
		$this->db->insert('tb_address',$data);
	}
}
