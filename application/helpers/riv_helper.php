<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for Rimiyong Page
 *
 * @package     CodeIgniter
 * @author      GOS
 * @copyright   Copyright (c) 2018 - 2020, EllisLab, Inc.
 * @since       Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Form Helpers
 *
 * @package     CodeIgniter
 * @subpackage  Helpers
 * @category    Helpers
 * @author      GOS
 */

// ------------------------------------------------------------------------

/**
 * Form Declaration
 *
 * Creates formSelect2Date
 *
 * @access  public
 * @param   string  the URI segments of the form destination
 * @param   array   a key/value pair of attributes
 * @param   array   a key/value pair hidden data
 * @return  string
 */


if ( !function_exists('clean')){ 
    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
}

if ( !function_exists('generate_token')){ 
    function generate_token() {
        return clean(md5(rand() . time() . rand()));
    }
}
