<?php
defined('BASEPATH') || exit('No direct script access allowed');

$config['encrypto'] = 'imap';
$config['validate'] = false;
$config['host']     = 'testmail.com';
$config['port']     = 143;
$config['username'] = 'user02@testmail.com';
$config['password'] = 'aaa';

$config['folders'] = [
	'inbox'  => 'INBOX',
	'sent'   => 'Sent',
	'trash'  => 'Trash',
	'spam'   => 'Spam',
	'drafts' => 'Drafts',
];

$config['expunge_on_disconnect'] = false;

$config['cache'] = [
	'active'     => false,
	'adapter'    => 'file',
	'backup'     => 'file',
	'key_prefix' => 'imap:',
	'ttl'        => 60,
];
