<?php
if( ! defined('BASEPATH')) exit('No direct script access allowed');


class Testimonials extends Web_Controller{

	private $viewfolder = 'admin/testimonials/';
	
	public function __construct(){
		parent::__construct();
		if($this->session->userdata('logged_in') != True)
        {
        	redirect('admin/login');
        }
        $this->load->model('Admin_model');
        $this->load->model('common', '', TRUE);
    }

	public function index(){
		$data = array();
		$this->load->view('admin/testimonials/list', $data);
	}
	
	public function get_list(){
		$data['data'] = array();
		$config["base_url"] = site_url() . "admin/testimonials/index";
        $total_row                  = $this->Admin_model->get_section_count('tb_testimonials');
		
		$config["total_rows"]       = $total_row;
        $config["per_page"]         = $_GET['pagination']['perpage'];
        $config["uri_segment"]      = 4;
        $this->pagination->initialize($config);
        $lastPage = ceil($total_row / $config["per_page"]);
        if ($this->uri->segment(4)) {
            $page = ($this->uri->segment(4));
        } else {
            $page = 1;
        }
         if($_GET['pagination']['perpage'] > $total_row){
            $data["meta"]["page"] = 1;    
        }
        else{
            $data["meta"]["page"] = $_GET['pagination']['page'];
        }
        $data["meta"]["perpage"] = $_GET['pagination']['perpage'];
        //$data["meta"]["pages"] = $_POST['pagination']['pages'];
        $data["meta"]["total"] = $total_row;
        if(isset($_GET['sort']['sort']))
            $data["meta"]["sort"] = $_GET['sort']['sort'];
        else
            $data["meta"]["sort"] = "asc";
        if(isset($_GET['sort']['field']))
            $data["meta"]["field"] = $_GET['sort']['field'];
        else
            $data["meta"]["field"] = "id";
        $data["data"] = $this->Admin_model->list_section((($_GET['pagination']['page']-1) * $_GET['pagination']['perpage']),$_GET['pagination']['perpage'],$data['meta']['field'],$data['meta']['sort'],'tb_testimonials');
        echo json_encode($data);
	}
	
	public function create(){
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			$data = array();
        	$data['description'] = $_POST['description'];
        	$data['create_date'] = date("Y-m-d H:i:s");
        	$this->Admin_model->insert('tb_testimonials',$data);
			$this->session->set_flashdata('succ_msg', 'Register Successfully.');
        	redirect('admin/testimonials');
        } else {
            $data['editMode'] = false;
            $this->load->view($this->viewfolder . 'edit', $data);
        }
	}
	
	public function edit($id){
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			$data = array();
            $data['id'] = $_POST['id'];
        	$data['description'] = $_POST['description'];
        	$this->Admin_model->update('tb_testimonials',$data);
			$this->session->set_flashdata('succ_msg', 'Update Successfully.');
        	redirect('admin/testimonials');		
			
        } else {
            $data['editMode'] = true;
            $data['result'] = $this->Admin_model->get_item('tb_testimonials',array('id'=>$id));
            $this->load->view($this->viewfolder . 'edit', $data);
        }
	}
	
	public function remove($id){
		$isRemoved = $this->Admin_model->delete('tb_testimonials',$id);
		if($isRemoved) {
            $this->session->set_flashdata('succ_msg', 'Delete Successfully');
        } else {
            $this->session->set_flashdata('error_msg', 'Delete Fail.');
        }
        redirect('admin/testimonials');
	}
	
	public function remove_multi(){
		$data = $_POST['data'];
		foreach($data as $key => $value){
			$this->Admin_model->delete('tb_testimonials',$value);
		}
		$result['status'] = 1;
		echo json_encode($result);
	}
	
}
?>
