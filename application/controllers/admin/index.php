<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Web_Controller {

	public function __construct() {
		parent::__construct();
		
		if($this->session->userdata('logged_in') != True)
        {
            return redirect(site_url('admin/login'));
        }
        else{
			return redirect(site_url('Center/lists'));
		}
 	}
 	public function index(){
		
		if(isset($_POST)) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required|callback_check_login');                 
			if($this->form_validation->run() == TRUE) {
				redirect(base_url().'customer/customer_details');
			}
		}
		$template['page_title'] = "Login";
		
		$this->load->view('Templates/Admin/header', $template);
		$this->load->view('Admin/login/login-form');
		$this->load->view('Templates/Admin/custom-login');
		
		
		
	}
}
	