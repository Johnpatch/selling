<?php
if( ! defined('BASEPATH')) exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Login extends Web_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Admin_model');
    }

	public function index(){
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == FALSE) 
        {   
            $data['result'] = $this->input->post();
            $this->load->view('admin/login/index',$data);
        }
        else
        {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            
            $result = $this->Admin_model->login($email, $password);

            if($result)
            {
                $this->user_session_create($result);
                redirect('admin/main');
            }
            else
            {
            	$data['result'] = $this->input->post();
                $this->session->set_flashdata('error_msg','User Name or Password Error');
                $this->load->view('admin/login/index',$data);
            }           
        }  
	}

	
	private function user_session_create($user){
		$this->session->set_userdata(array(
			'id' 		=> $user['id'],
			'user_type' => $user['user_type'],
			'email'		=> $user['email'],
			'firstname'	=> $user['firstname'], 
			'lastname'	=> $user['lastname'], 
			'logged_in' => true
		));
	}

	public function signout(){
		$this->session->sess_destroy();
		redirect(site_url('admin/login'));
	}
	
	public function change_password(){
		$data['user_pwd'] = $this->session->userdata('password');
		$this->load->view('admin/login/change_password', $data);
	}
	
	public function reset_password(){
		$request = $this->input->post();
		$response = array();
		$curtime = date('Y-m-d H:i:s');

		$this->db->where('f_no', $request['user_id']);
		$this->db->set('f_pwd', $request['new_password']);
		$this->db->set('f_updatetime', $curtime);
		$this->db->update('base_user');
		
		$this->session->set_userdata('password', $request['new_password']);
		
		$response['message'] = 'success';
		echo json_encode($response);
	}
}
?>
