<?php
if( ! defined('BASEPATH')) exit('No direct script access allowed');


class Users extends Web_Controller{

	private $viewfolder = 'admin/users/';
	
	public function __construct(){
		parent::__construct();
		if($this->session->userdata('logged_in') != True)
        {
        	redirect('admin/login');
        }
        $this->load->model('Admin_model');
        $this->load->model('common', '', TRUE);
    }

	public function index(){
		$data['users'] = $this->Admin_model->get_data('tb_user');
		$this->load->view('admin/users/list', $data);
	}
	
	public function create(){
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			if(!empty($_FILES)){
				$data = array();
				if (!empty($_FILES['avatar']) && $_FILES['avatar']['size'] > 0) 
	            {   
	                $this->common->upload($_FILES['avatar'], "avatar", "./upload/users/");
	                $data['avatar'] = $_FILES['avatar']['name'];
	            }else{
					$data['avatar'] = 'default.png';
				}
            	$data['firstname'] = $_POST['firstname'];
	        	$data['email'] = $_POST['email'];
	        	$data['lastname'] = $_POST['lastname'];
	        	$data['active'] = $_POST['active'];
            	$this->Admin_model->insert('tb_user',$data);
				$this->session->set_flashdata('succ_msg', 'Register Successfully');
            	redirect('admin/users');
			}
        } else {
            $data['editMode'] = false;
            $this->load->view($this->viewfolder . 'edit', $data);
        }
	}
	
	public function edit($id = null){
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			
			$data = array();
			if (!empty($_FILES['avatar']) && $_FILES['avatar']['size'] > 0) 
            {   
                $this->common->upload($_FILES['avatar'], "avatar", "./upload/users/");
                $data['avatar'] = $_FILES['avatar']['name'];
            }
            
        	$data['id'] = $_POST['id'];
        	$data['firstname'] = $_POST['firstname'];
        	$data['email'] = $_POST['email'];
        	$data['lastname'] = $_POST['lastname'];
        	$data['active'] = $_POST['active'];
        	
        	$this->Admin_model->update('tb_user',$data);
			$this->session->set_flashdata('succ_msg', 'Update Successfully');
        	redirect('admin/users');		
			
        } else {
            $data['editMode'] = true;
            $data['result'] = $this->Admin_model->get_item('tb_user',array('id'=>$id));
            $this->load->view($this->viewfolder . 'edit', $data);
        }
	}
	
	public function update_pwd(){
		$data = array();
		$admin = $this->Admin_model->get_admin_info();
		if($admin['password'] != sha1($_POST['old_password'])){
			$data['message'] = 'old_error';
			echo json_encode($data);
		} else if($_POST['new_password'] != $_POST['confirm_password']){
			$data['message'] = 'new_error';
			echo json_encode($data);
		}else{
			$this->Admin_model->update_admin_password($_POST['new_password']);
			$data['message'] = 'success';
			echo json_encode($data);	
		}
	}
	
	public function remove($id){
		$isRemoved = $this->Admin_model->delete('tb_user',$id);
		if($isRemoved) {
            $this->session->set_flashdata('succ_msg', 'Delete Successfully');
        } else {
            $this->session->set_flashdata('error_msg', 'Delete Fail');
        }

        redirect('admin/users');
	}
	
	function logout()
    {
        $this->session->sess_destroy();
        redirect('admin/login');
    }
    
    function profile(){
    	$data = array();
		$data['admin'] = $this->Admin_model->get_admin_info();
		$this->load->view($this->viewfolder.'profile',$data);
	}
}
?>
