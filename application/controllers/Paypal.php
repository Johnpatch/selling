<?php

class Paypal extends Web_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->library('paypal_lib');
        $this->load->model('Home_model');
        include_once(APPPATH."libraries/phpmailer/class.phpmailer.php");
		include_once(APPPATH."libraries/phpmailer/class.smtp.php");
    }
    
    function create_payment_with_paypal(){
		$returnURL = base_url().'paypal/success?product_id='.$_GET['product_id'].'&email='.$_GET['email'];
	    $cancelURL = base_url().'paypal/cancel?product_id='.$_GET['product_id'].'&email='.$_GET['email'];
	    
	    $product_info = $this->Home_model->get_data('tb_product',array('is_show'=>1,'id'=>$_GET['product_id']));
		
	    // Add fields to paypal form
	    $this->paypal_lib->add_field('return', $returnURL);
	    $this->paypal_lib->add_field('cancel_return', $cancelURL);
	    $this->paypal_lib->add_field('item_name', $product_info[0]['name']);
	    $this->paypal_lib->add_field('member', 1);
	    $this->paypal_lib->add_field('item_number',  $product_info[0]['id']);
	    $this->paypal_lib->add_field('amount',  $product_info[0]['price']);
	    
	    // Render paypal form
	    $this->paypal_lib->paypal_auto_form();
	}

    function success() {
    	$product_info = $this->Home_model->get_data('tb_product',array('is_show'=>1,'id'=>$_GET['product_id']));
        $this->load->config('email');
        $from = $this->config->item('no_reply');
        $to = $_GET['email'];
        $subject = 'Thanks for your Order. Please find your Certification Smart Kit Attached';
         $message = 'Dear,<br> Thanks for purchasing with us. You have taken the first step to grow your business. Please find attached the copy of your '.$product_info[0]['name'].' below<br> '.$product_info[0]['description'];
		$this->sendEmail($from,'ALLCERFITY',$to,$subject,$message,$product_info[0]['file_name']);
    	redirect('Home/index');
    }

    function cancel() {
    	$this->load->config('email');
    	$from = $this->config->item('notification');
        $to = $_POST['email'];
        $subject = 'Transaction Fail';
        $message = 'Your transaction could not be processed, please review your information and try again.';
		$this->sendEmail($from,'ALLCERFITY',$to,$subject,$message,'');
			
		$this->load_view('paypal/cancel');
    }

    function sendEmail($from, $fromName, $to, $subject, $content, $attach){
    	$this->load->config('email');
		$mail = new PHPMailer;

		$mail->isSMTP();
		$mail->setFrom($from,$fromName);
		$mail->addAddress($to);
		
		$mail->Username = $this->config->item('smtp_user');
		$mail->Password = $this->config->item('smtp_pass');
		$mail->Host = $this->config->item('smtp_host');
		
		$mail->Subject = $subject;
		$mail->Body = $content;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls';
		$mail->Port = 587;
		$mail->$mail->IsHTML(true);
		$mail->addAttachment('upload/pdf/'.$attach);
		
		$mail->send();
	}
}