<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Web_Controller {

    public function __construct() {
        $this->mainTitle = 'home';
        parent::__construct();
        $this->load->model('Home_model');
        include_once(APPPATH."libraries/phpmailer/class.phpmailer.php");
		include_once(APPPATH."libraries/phpmailer/class.smtp.php");
    }

    public function index() {

        $req = $this->input->get();

        if (!isset($req['lang']))
            $lang = '';
        else
            $lang = $req['lang'];

        $this->set_lang($lang);

        $view_params = array();
        $view_params['products'] = $this->Home_model->get_data('tb_product',array('is_show'=>1));
        $view_params['articles'] = $this->Home_model->get_articles('tb_articles',array('is_show'=>1));
        $view_params['main'] = $this->Home_model->get_data('tb_main',array('is_show'=>1));
        $view_params['testimonials'] = $this->Home_model->get_data('tb_testimonials',array('is_show'=>1));
        $view_params['about'] = $this->Home_model->get_data('tb_about',array('is_show'=>1));
        $view_params['benefits'] = $this->Home_model->get_data('tb_benefits',array('is_show'=>1));
        $this->load_view('home/index', $view_params);
        
        $this->load_view_self('template/main', array('step' => 0));
    }

    public function ajax_change_language() {

        $req = $this->input->post();
        $lang = $req['lang'];
        $this->set_lang($lang);
        echo json_encode(['result' => 'success']);
    }
    
    public function paypal_entry(){
		include_once(APPPATH."libraries/CallerService.php");
		$paymentType =urlencode( $_POST['paymentType']);
		$firstName =urlencode( $_POST['fname']);
		$lastName =urlencode( $_POST['lname']);
		$creditCardType =urlencode( $_POST['creditCardType']);
		$creditCardNumber = urlencode($_POST['creditCardNumber']);
		$expDateMonth =urlencode( $_POST['expDateMonth']);

		//Month must be padded with leading zero
		$padDateMonth = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);

		$expDateYear =urlencode( $_POST['expDateYear']);
		$cvv2Number = urlencode($_POST['cvv2Number']);
		$amount = urlencode($_POST['ftotal']);
		$currencyCode="USD";

		/* Construct the request string that will be sent to PayPal.
		   The variable $nvpstr contains all the variables and is a
		   name value pair string with & as a delimiter */

		$nvpstr="&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".$padDateMonth.$expDateYear."&CVV2=$cvv2Number&FIRSTNAME=$firstName&COUNTRYCODE=US&CURRENCYCODE=$currencyCode";

		/* Make the API call to PayPal, using API signature.
		   The API response is stored in an associative array called $resArray */
		$resArray=hash_call("doDirectPayment",$nvpstr);

		/* Display the API response back to the browser.
		   If the response from PayPal was a success, display the response parameters'
		   If the response was an error, display the errors received using APIError.php.
		*/

		$ack = strtoupper($resArray["ACK"]);
		$this->load->config('email');
		if($ack!="SUCCESS")  {
			$from = $this->config->item('notification');
	        $to = $_POST['email'];
	        $subject = 'Transaction Fail';
	        $message = 'Your transaction could not be processed, please review your information and try again.';
			$this->sendEmail($from,'ALLCERFITY',$to,$subject,$message,'');
			
			//redirect('Home/fail?product_id='.$_POST['p_id'].'&email='.$_POST['email']);
			$result['result'] = 'fail';
			echo json_encode($result);
		}
		if($ack=="SUCCESS")  {
			$product_info = $this->Home_model->get_data('tb_product',array('is_show'=>1,'id'=>$_POST['p_id']));
			
			$from = $this->config->item('no_reply');
	        $to = $_POST['email'];
	        $subject = 'Thanks for your Order. Please find your Certification Smart Kit Attached';
	        $message = 'Dear, <br>Thanks for purchasing with us. You have taken the first step to grow your business. Please find attached the copy of your '.$product_info[0]['name'].' below<br> '.$product_info[0]['description'];
			$this->sendEmail($from,'ALLCERFITY',$to,$subject,$message,$product_info[0]['file_name']);
			
			$result['result'] = 'success';
			echo json_encode($result);
			
		}
	}
	
	public function success(){
		$this->load_view('home/success', array());
        $this->load_view_self('template/main', array('step' => 0));
	}
	
	public function terms(){
		$this->load_view('home/terms', array());
        $this->load_view_self('template/main', array('step' => 0));
	}
	
	public function view_article($id){
		$article_info = $this->Home_model->get_data('tb_articles',array('is_show'=>1,'id'=>$id));
		$this->load_view('home/article', array('article'=>$article_info[0]));
        $this->load_view_self('template/main', array('step' => 0));
	}

	public function fail(){
		$data = array();
		$data['p_id'] = $_GET['product_id'];
		$data['email'] = $_GET['email'];
		$this->load_view('home/fail', $data);
        $this->load_view_self('template/main', array('step' => 0));
	}
	
	function sendEmail($from, $fromName, $to, $subject, $content, $attach){
		$this->load->config('email');
		
		$mail = new PHPMailer;

		$mail->isSMTP();
		$mail->setFrom($from,$fromName);
		$mail->addAddress($to);
		
		$mail->Username = $this->config->item('smtp_user');
		$mail->Password = $this->config->item('smtp_pass');
		$mail->Host = $this->config->item('smtp_host');
		
		$mail->Subject = $subject;
		$mail->Body = $content;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls';
		$mail->Port = 587;
		$mail->IsHTML(true);
		$mail->addAttachment('upload/pdf/'.$attach);
		
		$mail->send();
	}
}