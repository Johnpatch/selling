<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Web_Controller extends CI_Controller {

    public $custom_css_list = array();
    public $custom_js_list = array();
    public $custom_jslink_list = array();
    public $view_list = array();

    public $mainTitle = '';
    public $subTitle = '';

	public $isLogin = FALSE;	
	public $userData = null;
	public $userType = "";
	public $isAdmin = FALSE;

	public $env = array();

    // Jackson
    public $opposite = array();

    // language

    public $cur_lang = 1;
    public $cur_langname;
    public $lang_words = array();

	public function __construct() {
		parent::__construct();

        $this->load->model('PM_Model');

        $this->set_lang($this->session->userdata('language'));

		$this->isLogin = $this->session->userdata('user_data') ? true : false;
        $this->isAdmin = FALSE;

		if ( $this->isLogin )
		{
			$this->userData = $this->session->userdata('user_data');

            if (!$this->userData->avatar || !file_exists('./upload/users/'.$this->userData->avatar))
                $this->userData->avatar = 'default.png';
        }
	}
    
    function load_view_self($view, $vars = array(), $return = FALSE) {
        $vars['self'] = $this;
        $vars['languages'] = $this->PM_Model->get_list('base_lang', 0, '', '', '', 'f_shortname, f_name, f_flag');
        return $this->load->view($view, $vars, $return);
    }

    public function _l($key) {
        if (isset($this->lang_words[$key]))
            return $this->lang_words[$key];
        return $key;
    }

    function set_lang($lang_name) {

        if ($lang_name == '')
            $lang_name = $this->session->userdata('language');

        $lang = $this->PM_Model->get_info_with_where('base_lang', 'f_shortname="'.$lang_name.'"');

        $this->cur_langname = "English";
        
        if (!isset($lang)) {
            $this->cur_lang = 1;
            $this->session->set_userdata('language', 'ja');

            if ($lang_name != "")
                redirect('/?lang=en');

        } else {
            $this->cur_lang = $lang->f_no;
            $this->cur_langname = $lang->f_name;
        }

        $this->lang_words = array();
        $this->session->set_userdata('language', $lang_name);

        //////////////               otherwise english                    //////////////////////

        $lang_words = $this->PM_Model->get_list('lang_words', 0, 'f_lang=1', '', '', 'f_key, f_content');
        foreach ($lang_words as $word) {
            $this->lang_words[$word['f_key']] = $word['f_content'];
        }

        if ($this->cur_lang != 1)

        ////////////////             otherwise english end                /////////////////////////
        {
            $lang_words = $this->PM_Model->get_list('lang_words', 0, 'f_lang='.$this->cur_lang, '', '', 'f_key, f_content');
            foreach ($lang_words as $word) {
                if ($word['f_content'] != '')
                    $this->lang_words[$word['f_key']] = $word['f_content'];
            }
        }

    }

	public function __call($method, $arguments) {
        if ($method === 'load_css') {
            $this->custom_css_list[] = $arguments[0];
        } elseif ($method === 'load_js') {
            $this->custom_js_list[] = $arguments[0];
        } elseif ($method == 'load_jslink') {
            $this->custom_jslink_list[] = $arguments[0];
        } elseif ($method === 'load_view') {
            $this->view_list[] = $arguments;
        } else {
            die("<p>" . $method . " doesn't exist</p>");
        }
	}

}